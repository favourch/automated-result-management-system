namespace ARMS
{
    partial class homeBursar
    {
        /*
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(homeBursar));
            this.panel2 = new System.Windows.Forms.Panel();
            this.digitalClockCtrl1 = new DigiClock.DigitalClockCtrl();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolBtnMsg = new System.Windows.Forms.ToolStripButton();
            this.searchName = new System.Windows.Forms.ToolStripTextBox();
            this.btnAddView = new System.Windows.Forms.ToolStripButton();
            this.searchText = new System.Windows.Forms.ToolStripTextBox();
            this.btnFind = new System.Windows.Forms.ToolStripButton();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.regno = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.phone = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.email = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnLock = new ARMS.RibbonMenuButton();
            this.btnMakePayment = new ARMS.RibbonMenuButton();
            this.btnPhistory = new ARMS.RibbonMenuButton();
            this.btnMessage = new ARMS.RibbonMenuButton();
            this.panel3 = new System.Windows.Forms.Panel();
            this.cbolevel = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cbofeetype = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.Color.SkyBlue;
            this.panel2.Controls.Add(this.digitalClockCtrl1);
            this.panel2.Controls.Add(this.toolStrip1);
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(683, 62);
            this.panel2.TabIndex = 4;
            // 
            // digitalClockCtrl1
            // 
            this.digitalClockCtrl1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.digitalClockCtrl1.BackColor = System.Drawing.Color.Transparent;
            this.digitalClockCtrl1.CountDownTime = 10000;
            this.digitalClockCtrl1.Location = new System.Drawing.Point(545, 14);
            this.digitalClockCtrl1.Name = "digitalClockCtrl1";
            this.digitalClockCtrl1.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.digitalClockCtrl1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.digitalClockCtrl1.SetClockType = DigiClock.ClockType.DigitalClock;
            this.digitalClockCtrl1.Size = new System.Drawing.Size(126, 37);
            this.digitalClockCtrl1.TabIndex = 1;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.toolStrip1.AutoSize = false;
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.Font = new System.Drawing.Font("Trebuchet MS", 11F);
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(48, 48);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolBtnMsg,
            this.searchName,
            this.btnAddView,
            this.searchText,
            this.btnFind});
            this.toolStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.toolStrip1.Location = new System.Drawing.Point(3, 2);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(534, 57);
            this.toolStrip1.TabIndex = 2;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolBtnMsg
            // 
            this.toolBtnMsg.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolBtnMsg.Image = global::ARMS.Properties.Resources.chat;
            this.toolBtnMsg.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolBtnMsg.Name = "toolBtnMsg";
            this.toolBtnMsg.Size = new System.Drawing.Size(52, 54);
            this.toolBtnMsg.Text = "Messages";
            this.toolBtnMsg.ToolTipText = "Messages";
            this.toolBtnMsg.Click += new System.EventHandler(this.btnMessage_Click);
            // 
            // searchName
            // 
            this.searchName.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.searchName.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.RecentlyUsedList;
            this.searchName.AutoSize = false;
            this.searchName.Name = "searchName";
            this.searchName.Size = new System.Drawing.Size(150, 57);
            this.searchName.Text = "Search by Last Name";
            this.searchName.ToolTipText = "Search by Last Name.";
            this.searchName.Click += new System.EventHandler(this.searchText_Click);
            // 
            // btnAddView
            // 
            this.btnAddView.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAddView.Image = global::ARMS.Properties.Resources.viewmagfit;
            this.btnAddView.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAddView.Name = "btnAddView";
            this.btnAddView.Size = new System.Drawing.Size(52, 54);
            this.btnAddView.Text = "Search students by Name";
            this.btnAddView.Click += new System.EventHandler(this.btnAddView_Click);
            // 
            // searchText
            // 
            this.searchText.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.searchText.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.RecentlyUsedList;
            this.searchText.AutoSize = false;
            this.searchText.Name = "searchText";
            this.searchText.Size = new System.Drawing.Size(150, 57);
            this.searchText.Text = "Search by Reg. No.";
            this.searchText.ToolTipText = "Search by Reg. No.";
            this.searchText.Click += new System.EventHandler(this.searchText_Click);
            // 
            // btnFind
            // 
            this.btnFind.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnFind.Image = global::ARMS.Properties.Resources.find;
            this.btnFind.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnFind.Name = "btnFind";
            this.btnFind.Size = new System.Drawing.Size(52, 54);
            this.btnFind.Text = "Find Student By Reg No.";
            this.btnFind.Click += new System.EventHandler(this.btnFind_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.regno,
            this.fname,
            this.lname,
            this.mName,
            this.phone,
            this.email});
            this.dataGridView1.Location = new System.Drawing.Point(0, 62);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(683, 250);
            this.dataGridView1.TabIndex = 5;
            this.dataGridView1.SelectionChanged += new System.EventHandler(this.dataGridView1_SelectionChanged);
            // 
            // regno
            // 
            this.regno.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.regno.DataPropertyName = "regno";
            this.regno.HeaderText = "Reg. No.";
            this.regno.Name = "regno";
            this.regno.ReadOnly = true;
            // 
            // fname
            // 
            this.fname.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.fname.DataPropertyName = "firstname";
            this.fname.HeaderText = "Firstname";
            this.fname.Name = "fname";
            this.fname.ReadOnly = true;
            // 
            // lname
            // 
            this.lname.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.lname.DataPropertyName = "lastname";
            this.lname.HeaderText = "Lastname";
            this.lname.Name = "lname";
            this.lname.ReadOnly = true;
            // 
            // mName
            // 
            this.mName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.mName.DataPropertyName = "middlename";
            this.mName.HeaderText = "Middlename";
            this.mName.Name = "mName";
            this.mName.ReadOnly = true;
            // 
            // phone
            // 
            this.phone.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.phone.DataPropertyName = "phone";
            this.phone.HeaderText = "Phone";
            this.phone.Name = "phone";
            this.phone.ReadOnly = true;
            // 
            // email
            // 
            this.email.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.email.DataPropertyName = "email";
            this.email.HeaderText = "Email";
            this.email.Name = "email";
            this.email.ReadOnly = true;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.btnLock);
            this.panel1.Controls.Add(this.btnMakePayment);
            this.panel1.Controls.Add(this.btnPhistory);
            this.panel1.Controls.Add(this.btnMessage);
            this.panel1.Location = new System.Drawing.Point(150, 354);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(527, 106);
            this.panel1.TabIndex = 9;
            // 
            // btnLock
            // 
            this.btnLock.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLock.Arrow = ARMS.RibbonMenuButton.e_arrow.None;
            this.btnLock.BackColor = System.Drawing.Color.Transparent;
            this.btnLock.ColorBase = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnLock.ColorBaseStroke = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnLock.ColorOn = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnLock.ColorOnStroke = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnLock.ColorPress = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btnLock.ColorPressStroke = System.Drawing.Color.Aqua;
            this.btnLock.FadingSpeed = 35;
            this.btnLock.FlatAppearance.BorderSize = 0;
            this.btnLock.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLock.Font = new System.Drawing.Font("Trebuchet MS", 11F, System.Drawing.FontStyle.Bold);
            this.btnLock.GroupPos = ARMS.RibbonMenuButton.e_groupPos.None;
            this.btnLock.Image = global::ARMS.Properties.Resources.Lock;
            this.btnLock.ImageLocation = ARMS.RibbonMenuButton.e_imagelocation.Top;
            this.btnLock.ImageOffset = 5;
            this.btnLock.IsPressed = false;
            this.btnLock.KeepPress = false;
            this.btnLock.Location = new System.Drawing.Point(7, 3);
            this.btnLock.MaxImageSize = new System.Drawing.Point(64, 64);
            this.btnLock.MenuPos = new System.Drawing.Point(0, 0);
            this.btnLock.Name = "btnLock";
            this.btnLock.Radius = 6;
            this.btnLock.ShowBase = ARMS.RibbonMenuButton.e_showbase.Yes;
            this.btnLock.Size = new System.Drawing.Size(124, 100);
            this.btnLock.SplitButton = ARMS.RibbonMenuButton.e_splitbutton.No;
            this.btnLock.SplitDistance = 0;
            this.btnLock.TabIndex = 8;
            this.btnLock.Text = "Lock Student";
            this.btnLock.Title = "";
            this.btnLock.UseVisualStyleBackColor = false;
            this.btnLock.Click += new System.EventHandler(this.btnLock_Click);
            // 
            // btnMakePayment
            // 
            this.btnMakePayment.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMakePayment.Arrow = ARMS.RibbonMenuButton.e_arrow.None;
            this.btnMakePayment.BackColor = System.Drawing.Color.Transparent;
            this.btnMakePayment.ColorBase = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnMakePayment.ColorBaseStroke = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnMakePayment.ColorOn = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnMakePayment.ColorOnStroke = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnMakePayment.ColorPress = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btnMakePayment.ColorPressStroke = System.Drawing.Color.Aqua;
            this.btnMakePayment.FadingSpeed = 35;
            this.btnMakePayment.FlatAppearance.BorderSize = 0;
            this.btnMakePayment.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMakePayment.Font = new System.Drawing.Font("Trebuchet MS", 11F, System.Drawing.FontStyle.Bold);
            this.btnMakePayment.GroupPos = ARMS.RibbonMenuButton.e_groupPos.None;
            this.btnMakePayment.Image = global::ARMS.Properties.Resources.Money;
            this.btnMakePayment.ImageLocation = ARMS.RibbonMenuButton.e_imagelocation.Top;
            this.btnMakePayment.ImageOffset = 5;
            this.btnMakePayment.IsPressed = false;
            this.btnMakePayment.KeepPress = false;
            this.btnMakePayment.Location = new System.Drawing.Point(137, 3);
            this.btnMakePayment.MaxImageSize = new System.Drawing.Point(64, 64);
            this.btnMakePayment.MenuPos = new System.Drawing.Point(0, 0);
            this.btnMakePayment.Name = "btnMakePayment";
            this.btnMakePayment.Radius = 6;
            this.btnMakePayment.ShowBase = ARMS.RibbonMenuButton.e_showbase.Yes;
            this.btnMakePayment.Size = new System.Drawing.Size(124, 100);
            this.btnMakePayment.SplitButton = ARMS.RibbonMenuButton.e_splitbutton.No;
            this.btnMakePayment.SplitDistance = 0;
            this.btnMakePayment.TabIndex = 9;
            this.btnMakePayment.Text = "Payments";
            this.btnMakePayment.Title = "";
            this.btnMakePayment.UseVisualStyleBackColor = false;
            this.btnMakePayment.Click += new System.EventHandler(this.btnMakePayment_Click);
            // 
            // btnPhistory
            // 
            this.btnPhistory.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPhistory.Arrow = ARMS.RibbonMenuButton.e_arrow.None;
            this.btnPhistory.BackColor = System.Drawing.Color.Transparent;
            this.btnPhistory.ColorBase = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnPhistory.ColorBaseStroke = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnPhistory.ColorOn = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnPhistory.ColorOnStroke = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnPhistory.ColorPress = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btnPhistory.ColorPressStroke = System.Drawing.Color.Aqua;
            this.btnPhistory.FadingSpeed = 35;
            this.btnPhistory.FlatAppearance.BorderSize = 0;
            this.btnPhistory.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPhistory.Font = new System.Drawing.Font("Trebuchet MS", 11F, System.Drawing.FontStyle.Bold);
            this.btnPhistory.GroupPos = ARMS.RibbonMenuButton.e_groupPos.None;
            this.btnPhistory.Image = global::ARMS.Properties.Resources.Documents_Folder;
            this.btnPhistory.ImageLocation = ARMS.RibbonMenuButton.e_imagelocation.Top;
            this.btnPhistory.ImageOffset = 5;
            this.btnPhistory.IsPressed = false;
            this.btnPhistory.KeepPress = false;
            this.btnPhistory.Location = new System.Drawing.Point(267, 3);
            this.btnPhistory.MaxImageSize = new System.Drawing.Point(64, 64);
            this.btnPhistory.MenuPos = new System.Drawing.Point(0, 0);
            this.btnPhistory.Name = "btnPhistory";
            this.btnPhistory.Radius = 6;
            this.btnPhistory.ShowBase = ARMS.RibbonMenuButton.e_showbase.Yes;
            this.btnPhistory.Size = new System.Drawing.Size(124, 100);
            this.btnPhistory.SplitButton = ARMS.RibbonMenuButton.e_splitbutton.No;
            this.btnPhistory.SplitDistance = 0;
            this.btnPhistory.TabIndex = 6;
            this.btnPhistory.Text = "Payment History";
            this.btnPhistory.Title = "";
            this.btnPhistory.UseVisualStyleBackColor = false;
            this.btnPhistory.Click += new System.EventHandler(this.btnPhistory_Click);
            // 
            // btnMessage
            // 
            this.btnMessage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMessage.Arrow = ARMS.RibbonMenuButton.e_arrow.None;
            this.btnMessage.BackColor = System.Drawing.Color.Transparent;
            this.btnMessage.ColorBase = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnMessage.ColorBaseStroke = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnMessage.ColorOn = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnMessage.ColorOnStroke = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnMessage.ColorPress = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btnMessage.ColorPressStroke = System.Drawing.Color.Aqua;
            this.btnMessage.FadingSpeed = 35;
            this.btnMessage.FlatAppearance.BorderSize = 0;
            this.btnMessage.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMessage.Font = new System.Drawing.Font("Trebuchet MS", 11F, System.Drawing.FontStyle.Bold);
            this.btnMessage.GroupPos = ARMS.RibbonMenuButton.e_groupPos.None;
            this.btnMessage.Image = global::ARMS.Properties.Resources.chat;
            this.btnMessage.ImageLocation = ARMS.RibbonMenuButton.e_imagelocation.Top;
            this.btnMessage.ImageOffset = 5;
            this.btnMessage.IsPressed = false;
            this.btnMessage.KeepPress = false;
            this.btnMessage.Location = new System.Drawing.Point(397, 3);
            this.btnMessage.MaxImageSize = new System.Drawing.Point(64, 64);
            this.btnMessage.MenuPos = new System.Drawing.Point(0, 0);
            this.btnMessage.Name = "btnMessage";
            this.btnMessage.Radius = 6;
            this.btnMessage.ShowBase = ARMS.RibbonMenuButton.e_showbase.Yes;
            this.btnMessage.Size = new System.Drawing.Size(124, 100);
            this.btnMessage.SplitButton = ARMS.RibbonMenuButton.e_splitbutton.No;
            this.btnMessage.SplitDistance = 0;
            this.btnMessage.TabIndex = 7;
            this.btnMessage.Text = "Messages";
            this.btnMessage.Title = "";
            this.btnMessage.UseVisualStyleBackColor = false;
            this.btnMessage.Click += new System.EventHandler(this.btnMessage_Click);
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.Controls.Add(this.cbolevel);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.cbofeetype);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Location = new System.Drawing.Point(189, 315);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(488, 36);
            this.panel3.TabIndex = 10;
            // 
            // cbolevel
            // 
            this.cbolevel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cbolevel.Font = new System.Drawing.Font("Candara", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbolevel.FormattingEnabled = true;
            this.cbolevel.Location = new System.Drawing.Point(325, 8);
            this.cbolevel.Name = "cbolevel";
            this.cbolevel.Size = new System.Drawing.Size(150, 21);
            this.cbolevel.TabIndex = 3;
            this.cbolevel.Text = "Select level of payment";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(274, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Level :";
            // 
            // cbofeetype
            // 
            this.cbofeetype.Font = new System.Drawing.Font("Candara", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbofeetype.FormattingEnabled = true;
            this.cbofeetype.Items.AddRange(new object[] {
            "School Fees",
            "Transcript Fee",
            "Other Fee"});
            this.cbofeetype.Location = new System.Drawing.Point(113, 7);
            this.cbofeetype.Name = "cbofeetype";
            this.cbofeetype.Size = new System.Drawing.Size(140, 21);
            this.cbofeetype.TabIndex = 1;
            this.cbofeetype.Text = "Select Fee";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(45, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Fee Type : ";
            // 
            // homeBursar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(683, 463);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.panel2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "homeBursar";
            this.Text = "Bursar Control Dashboard";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.panel2.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private DigiClock.DigitalClockCtrl digitalClockCtrl1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolBtnMsg;
        private System.Windows.Forms.ToolStripTextBox searchName;
        private System.Windows.Forms.ToolStripButton btnAddView;
        private System.Windows.Forms.ToolStripTextBox searchText;
        private System.Windows.Forms.ToolStripButton btnFind;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Panel panel1;
        private RibbonMenuButton btnLock;
        private RibbonMenuButton btnMakePayment;
        private RibbonMenuButton btnPhistory;
        private RibbonMenuButton btnMessage;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.ComboBox cbolevel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbofeetype;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewTextBoxColumn regno;
        private System.Windows.Forms.DataGridViewTextBoxColumn fname;
        private System.Windows.Forms.DataGridViewTextBoxColumn lname;
        private System.Windows.Forms.DataGridViewTextBoxColumn mName;
        private System.Windows.Forms.DataGridViewTextBoxColumn phone;
        private System.Windows.Forms.DataGridViewTextBoxColumn email;
         */
    }
}
