using System;
using System.Drawing;
using System.Windows.Forms;

namespace ARMS
{
    public partial class StubbornWindow : Form
    {
        public StubbornWindow(Messages m)
        {
            InitializeComponent();
            var pt = Screen.GetWorkingArea(this);
            DesktopLocation = new Point((pt.Width - Width), 0);
            fMessage = m;
            Text += m.Sender;
            richTextBox1.Text = m.Message;
            var rs = Helper.selectResults(new[] {"args", "argvalues"}, "critical", new[] {"id"}, new[] {m.Id}, "");
            args = rs[0][0].Split(',');
            argvalues = rs[0][1].Split(',');
        }

        private string[] args;
        private string[] argvalues;
        private bool resolved;

        private readonly Messages fMessage;

        private void StubbornWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = !resolved;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            label1.Text = "Checking for updates...";
            if (Helper.selectResults(new[] {fMessage.Field}, fMessage.Table, args, argvalues, "")[0][0] == fMessage.Expected)
            {
                Hide();
                Helper.Update("messages", new[] { "unread" }, new[] { "1" }, new[]{"id"}, new[] { fMessage.Id }, "");
                resolved = true;
                Close();
            }
            label1.Text = "This window will be closed automatically when the following task has been resolved:";
        }
    }
}
