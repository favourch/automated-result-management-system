namespace ARMS
{
    partial class HomeFaculty
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        System.Windows.Forms.DataVisualization.Charting.DataPoint dataPointA;
        System.Windows.Forms.DataVisualization.Charting.DataPoint dataPointB;
        System.Windows.Forms.DataVisualization.Charting.DataPoint dataPointC;
        System.Windows.Forms.DataVisualization.Charting.DataPoint dataPointD;
        System.Windows.Forms.DataVisualization.Charting.DataPoint dataPointE;
        System.Windows.Forms.DataVisualization.Charting.DataPoint dataPointF;
        System.Windows.Forms.DataVisualization.Charting.DataPoint dataPointM;

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint1 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0D, 30D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint2 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(20D, 50D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint3 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(40D, 120D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint4 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(60D, 80D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint5 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(80D, 20D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint6 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(100D, 20D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint7 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(120D, 10D);
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HomeFaculty));
            this.panel2 = new System.Windows.Forms.Panel();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolBtnChat = new System.Windows.Forms.ToolStripButton();
            this.toolBtnPerformance = new System.Windows.Forms.ToolStripButton();
            this.searchText = new System.Windows.Forms.ToolStripTextBox();
            this.toolBtnFind = new System.Windows.Forms.ToolStripButton();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnReject = new ARMS.RibbonMenuButton();
            this.btnApprove = new ARMS.RibbonMenuButton();
            this.btnPerformance = new ARMS.RibbonMenuButton();
            this.btnChat = new ARMS.RibbonMenuButton();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.panelSmart = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.picRating = new System.Windows.Forms.PictureBox();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.comboCourse = new System.Windows.Forms.ComboBox();
            this.comboLevel = new System.Windows.Forms.ComboBox();
            this.comboDept = new System.Windows.Forms.ComboBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.RegNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dept = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Session = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Grade = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Score = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Phone = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.panel2.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panelSmart.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picRating)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.SkyBlue;
            this.panel2.Controls.Add(this.toolStrip1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(654, 62);
            this.panel2.TabIndex = 2;
            // 
            // toolStrip1
            // 
            this.toolStrip1.AutoSize = false;
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.Font = new System.Drawing.Font("Trebuchet MS", 11F);
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(48, 48);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolBtnChat,
            this.toolBtnPerformance,
            this.searchText,
            this.toolBtnFind});
            this.toolStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.toolStrip1.Location = new System.Drawing.Point(3, 2);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(479, 57);
            this.toolStrip1.TabIndex = 2;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolBtnChat
            // 
            this.toolBtnChat.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolBtnChat.Image = global::ARMS.Properties.Resources.chat;
            this.toolBtnChat.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolBtnChat.Name = "toolBtnChat";
            this.toolBtnChat.Size = new System.Drawing.Size(52, 54);
            this.toolBtnChat.Text = "View Messages";
            this.toolBtnChat.Click += new System.EventHandler(this.btnMsg_Click);
            // 
            // toolBtnPerformance
            // 
            this.toolBtnPerformance.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolBtnPerformance.Image = global::ARMS.Properties.Resources.Performance;
            this.toolBtnPerformance.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolBtnPerformance.Name = "toolBtnPerformance";
            this.toolBtnPerformance.Size = new System.Drawing.Size(52, 54);
            this.toolBtnPerformance.Text = "Analyze Performance";
            this.toolBtnPerformance.Click += new System.EventHandler(this.btnViewStudents_Click);
            // 
            // searchText
            // 
            this.searchText.AutoSize = false;
            this.searchText.Name = "searchText";
            this.searchText.Size = new System.Drawing.Size(200, 57);
            this.searchText.Text = "Search by Course Code.";
            this.searchText.ToolTipText = "Search by Reg. No.";
            this.searchText.Click += new System.EventHandler(this.searchText_Click);
            // 
            // toolBtnFind
            // 
            this.toolBtnFind.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolBtnFind.Image = global::ARMS.Properties.Resources.find;
            this.toolBtnFind.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolBtnFind.Name = "toolBtnFind";
            this.toolBtnFind.Size = new System.Drawing.Size(52, 54);
            this.toolBtnFind.Text = "Click to Search";
            this.toolBtnFind.Click += new System.EventHandler(this.btnFind_Click);
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.Controls.Add(this.btnReject);
            this.panel3.Controls.Add(this.btnApprove);
            this.panel3.Controls.Add(this.btnPerformance);
            this.panel3.Controls.Add(this.btnChat);
            this.panel3.Location = new System.Drawing.Point(12, 423);
            this.panel3.Name = "panel3";
            this.panel3.Padding = new System.Windows.Forms.Padding(5);
            this.panel3.Size = new System.Drawing.Size(630, 127);
            this.panel3.TabIndex = 3;
            // 
            // btnReject
            // 
            this.btnReject.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnReject.Arrow = ARMS.RibbonMenuButton.e_arrow.None;
            this.btnReject.BackColor = System.Drawing.Color.Transparent;
            this.btnReject.ColorBase = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnReject.ColorBaseStroke = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnReject.ColorOn = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnReject.ColorOnStroke = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnReject.ColorPress = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btnReject.ColorPressStroke = System.Drawing.Color.Aqua;
            this.btnReject.FadingSpeed = 35;
            this.btnReject.FlatAppearance.BorderSize = 0;
            this.btnReject.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReject.Font = new System.Drawing.Font("Trebuchet MS", 11F, System.Drawing.FontStyle.Bold);
            this.btnReject.GroupPos = ARMS.RibbonMenuButton.e_groupPos.None;
            this.btnReject.Image = global::ARMS.Properties.Resources.Delete;
            this.btnReject.ImageLocation = ARMS.RibbonMenuButton.e_imagelocation.Top;
            this.btnReject.ImageOffset = 5;
            this.btnReject.IsPressed = false;
            this.btnReject.KeepPress = false;
            this.btnReject.Location = new System.Drawing.Point(478, 10);
            this.btnReject.MaxImageSize = new System.Drawing.Point(72, 72);
            this.btnReject.MenuPos = new System.Drawing.Point(0, 0);
            this.btnReject.Name = "btnReject";
            this.btnReject.Radius = 6;
            this.btnReject.ShowBase = ARMS.RibbonMenuButton.e_showbase.Yes;
            this.btnReject.Size = new System.Drawing.Size(143, 106);
            this.btnReject.SplitButton = ARMS.RibbonMenuButton.e_splitbutton.No;
            this.btnReject.SplitDistance = 0;
            this.btnReject.TabIndex = 0;
            this.btnReject.Text = "Reject Results";
            this.btnReject.Title = "";
            this.btnReject.UseVisualStyleBackColor = false;
            this.btnReject.Click += new System.EventHandler(this.btnReject_Click);
            // 
            // btnApprove
            // 
            this.btnApprove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnApprove.Arrow = ARMS.RibbonMenuButton.e_arrow.None;
            this.btnApprove.BackColor = System.Drawing.Color.Transparent;
            this.btnApprove.ColorBase = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnApprove.ColorBaseStroke = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnApprove.ColorOn = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnApprove.ColorOnStroke = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnApprove.ColorPress = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btnApprove.ColorPressStroke = System.Drawing.Color.Aqua;
            this.btnApprove.FadingSpeed = 35;
            this.btnApprove.FlatAppearance.BorderSize = 0;
            this.btnApprove.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnApprove.Font = new System.Drawing.Font("Trebuchet MS", 11F, System.Drawing.FontStyle.Bold);
            this.btnApprove.GroupPos = ARMS.RibbonMenuButton.e_groupPos.None;
            this.btnApprove.Image = global::ARMS.Properties.Resources.apply;
            this.btnApprove.ImageLocation = ARMS.RibbonMenuButton.e_imagelocation.Top;
            this.btnApprove.ImageOffset = 5;
            this.btnApprove.IsPressed = false;
            this.btnApprove.KeepPress = false;
            this.btnApprove.Location = new System.Drawing.Point(327, 10);
            this.btnApprove.MaxImageSize = new System.Drawing.Point(72, 72);
            this.btnApprove.MenuPos = new System.Drawing.Point(0, 0);
            this.btnApprove.Name = "btnApprove";
            this.btnApprove.Radius = 6;
            this.btnApprove.ShowBase = ARMS.RibbonMenuButton.e_showbase.Yes;
            this.btnApprove.Size = new System.Drawing.Size(143, 106);
            this.btnApprove.SplitButton = ARMS.RibbonMenuButton.e_splitbutton.No;
            this.btnApprove.SplitDistance = 0;
            this.btnApprove.TabIndex = 0;
            this.btnApprove.Text = "Approve Results";
            this.btnApprove.Title = "";
            this.btnApprove.UseVisualStyleBackColor = false;
            this.btnApprove.Click += new System.EventHandler(this.btnUpdateData_Click);
            // 
            // btnPerformance
            // 
            this.btnPerformance.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPerformance.Arrow = ARMS.RibbonMenuButton.e_arrow.None;
            this.btnPerformance.BackColor = System.Drawing.Color.Transparent;
            this.btnPerformance.ColorBase = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnPerformance.ColorBaseStroke = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnPerformance.ColorOn = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnPerformance.ColorOnStroke = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnPerformance.ColorPress = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btnPerformance.ColorPressStroke = System.Drawing.Color.Aqua;
            this.btnPerformance.FadingSpeed = 35;
            this.btnPerformance.FlatAppearance.BorderSize = 0;
            this.btnPerformance.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPerformance.Font = new System.Drawing.Font("Trebuchet MS", 11F, System.Drawing.FontStyle.Bold);
            this.btnPerformance.GroupPos = ARMS.RibbonMenuButton.e_groupPos.None;
            this.btnPerformance.Image = global::ARMS.Properties.Resources.Performance;
            this.btnPerformance.ImageLocation = ARMS.RibbonMenuButton.e_imagelocation.Top;
            this.btnPerformance.ImageOffset = 5;
            this.btnPerformance.IsPressed = false;
            this.btnPerformance.KeepPress = false;
            this.btnPerformance.Location = new System.Drawing.Point(173, 10);
            this.btnPerformance.MaxImageSize = new System.Drawing.Point(72, 72);
            this.btnPerformance.MenuPos = new System.Drawing.Point(0, 0);
            this.btnPerformance.Name = "btnPerformance";
            this.btnPerformance.Radius = 6;
            this.btnPerformance.ShowBase = ARMS.RibbonMenuButton.e_showbase.Yes;
            this.btnPerformance.Size = new System.Drawing.Size(143, 106);
            this.btnPerformance.SplitButton = ARMS.RibbonMenuButton.e_splitbutton.No;
            this.btnPerformance.SplitDistance = 0;
            this.btnPerformance.TabIndex = 0;
            this.btnPerformance.Text = "Performance";
            this.btnPerformance.Title = "";
            this.toolTip1.SetToolTip(this.btnPerformance, "Switch between different performance analysis previews");
            this.btnPerformance.UseVisualStyleBackColor = false;
            this.btnPerformance.Click += new System.EventHandler(this.btnViewStudents_Click);
            // 
            // btnChat
            // 
            this.btnChat.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnChat.Arrow = ARMS.RibbonMenuButton.e_arrow.None;
            this.btnChat.BackColor = System.Drawing.Color.Transparent;
            this.btnChat.ColorBase = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnChat.ColorBaseStroke = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnChat.ColorOn = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnChat.ColorOnStroke = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnChat.ColorPress = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btnChat.ColorPressStroke = System.Drawing.Color.Aqua;
            this.btnChat.FadingSpeed = 35;
            this.btnChat.FlatAppearance.BorderSize = 0;
            this.btnChat.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnChat.Font = new System.Drawing.Font("Trebuchet MS", 11F, System.Drawing.FontStyle.Bold);
            this.btnChat.GroupPos = ARMS.RibbonMenuButton.e_groupPos.None;
            this.btnChat.Image = global::ARMS.Properties.Resources.chat;
            this.btnChat.ImageLocation = ARMS.RibbonMenuButton.e_imagelocation.Top;
            this.btnChat.ImageOffset = 5;
            this.btnChat.IsPressed = false;
            this.btnChat.KeepPress = false;
            this.btnChat.Location = new System.Drawing.Point(15, 10);
            this.btnChat.Margin = new System.Windows.Forms.Padding(5);
            this.btnChat.MaxImageSize = new System.Drawing.Point(72, 72);
            this.btnChat.MenuPos = new System.Drawing.Point(0, 0);
            this.btnChat.Name = "btnChat";
            this.btnChat.Radius = 6;
            this.btnChat.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnChat.ShowBase = ARMS.RibbonMenuButton.e_showbase.Yes;
            this.btnChat.Size = new System.Drawing.Size(147, 106);
            this.btnChat.SplitButton = ARMS.RibbonMenuButton.e_splitbutton.No;
            this.btnChat.SplitDistance = 0;
            this.btnChat.TabIndex = 0;
            this.btnChat.Text = "Contact Lecturer";
            this.btnChat.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.btnChat.Title = "";
            this.btnChat.UseVisualStyleBackColor = false;
            this.btnChat.Click += new System.EventHandler(this.btnChat_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.panelSmart);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.comboCourse);
            this.panel1.Controls.Add(this.comboLevel);
            this.panel1.Controls.Add(this.comboDept);
            this.panel1.Controls.Add(this.chart1);
            this.panel1.Controls.Add(this.dataGridView1);
            this.panel1.Location = new System.Drawing.Point(12, 68);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(630, 349);
            this.panel1.TabIndex = 10;
            // 
            // panelSmart
            // 
            this.panelSmart.AutoSize = true;
            this.panelSmart.Controls.Add(this.flowLayoutPanel2);
            this.panelSmart.Controls.Add(this.flowLayoutPanel3);
            this.panelSmart.Location = new System.Drawing.Point(626, 8);
            this.panelSmart.Name = "panelSmart";
            this.panelSmart.Size = new System.Drawing.Size(204, 39);
            this.panelSmart.TabIndex = 12;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.AutoSize = true;
            this.flowLayoutPanel2.Controls.Add(this.panel4);
            this.flowLayoutPanel2.Controls.Add(this.label3);
            this.flowLayoutPanel2.Controls.Add(this.picRating);
            this.flowLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(192, 33);
            this.flowLayoutPanel2.TabIndex = 10;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.label2);
            this.panel4.Location = new System.Drawing.Point(3, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(92, 27);
            this.panel4.TabIndex = 12;
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 27);
            this.label2.TabIndex = 0;
            this.label2.Text = "Smart Advisor Rating:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Candara", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Green;
            this.label3.Location = new System.Drawing.Point(101, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 26);
            this.label3.TabIndex = 12;
            this.label3.Text = "Good";
            // 
            // picRating
            // 
            this.picRating.Image = global::ARMS.Properties.Resources.apply;
            this.picRating.Location = new System.Drawing.Point(168, 3);
            this.picRating.Name = "picRating";
            this.picRating.Size = new System.Drawing.Size(21, 21);
            this.picRating.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picRating.TabIndex = 13;
            this.picRating.TabStop = false;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.AutoSize = true;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(201, 3);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(0, 0);
            this.flowLayoutPanel3.TabIndex = 11;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(401, 7);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(76, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Select Course:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(203, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Select Level:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 7);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(166, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Select department to view results:";
            // 
            // comboCourse
            // 
            this.comboCourse.Font = new System.Drawing.Font("Tahoma", 10F);
            this.comboCourse.FormattingEnabled = true;
            this.comboCourse.Location = new System.Drawing.Point(399, 23);
            this.comboCourse.Name = "comboCourse";
            this.comboCourse.Size = new System.Drawing.Size(221, 24);
            this.comboCourse.TabIndex = 4;
            this.comboCourse.SelectedIndexChanged += new System.EventHandler(this.comboCourse_SelectedIndexChanged);
            // 
            // comboLevel
            // 
            this.comboLevel.Font = new System.Drawing.Font("Tahoma", 10F);
            this.comboLevel.FormattingEnabled = true;
            this.comboLevel.Items.AddRange(new object[] {
            "100",
            "200",
            "300",
            "400",
            "500",
            "600"});
            this.comboLevel.Location = new System.Drawing.Point(206, 23);
            this.comboLevel.Name = "comboLevel";
            this.comboLevel.Size = new System.Drawing.Size(187, 24);
            this.comboLevel.TabIndex = 4;
            this.comboLevel.SelectedIndexChanged += new System.EventHandler(this.comboLevel_SelectedIndexChanged);
            // 
            // comboDept
            // 
            this.comboDept.Font = new System.Drawing.Font("Tahoma", 10F);
            this.comboDept.FormattingEnabled = true;
            this.comboDept.Items.AddRange(new object[] {
            "100",
            "200",
            "300",
            "400",
            "500",
            "600"});
            this.comboDept.Location = new System.Drawing.Point(13, 23);
            this.comboDept.Name = "comboDept";
            this.comboDept.Size = new System.Drawing.Size(187, 24);
            this.comboDept.TabIndex = 4;
            // 
            // dataGridView1
            // 
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Candara", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.RegNo,
            this.dept,
            this.Session,
            this.Grade,
            this.Score,
            this.Phone});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Candara", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.Location = new System.Drawing.Point(12, 55);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Candara", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridView1.Size = new System.Drawing.Size(608, 278);
            this.dataGridView1.TabIndex = 7;
            // 
            // RegNo
            // 
            this.RegNo.HeaderText = "Reg.No.";
            this.RegNo.Name = "RegNo";
            this.RegNo.ReadOnly = true;
            // 
            // dept
            // 
            this.dept.HeaderText = "Course";
            this.dept.Name = "dept";
            this.dept.ReadOnly = true;
            // 
            // Session
            // 
            this.Session.HeaderText = "CA Score";
            this.Session.Name = "Session";
            this.Session.ReadOnly = true;
            // 
            // Grade
            // 
            this.Grade.HeaderText = "Exam Score";
            this.Grade.Name = "Grade";
            this.Grade.ReadOnly = true;
            // 
            // Score
            // 
            this.Score.HeaderText = "Score";
            this.Score.Name = "Score";
            this.Score.ReadOnly = true;
            // 
            // Phone
            // 
            this.Phone.HeaderText = "Grade";
            this.Phone.Name = "Phone";
            this.Phone.ReadOnly = true;
            // 
            // chart1
            // 
            this.chart1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.chart1.BackColor = System.Drawing.Color.Transparent;
            this.chart1.BorderSkin.BackColor = System.Drawing.Color.Azure;
            this.chart1.BorderSkin.BackGradientStyle = System.Windows.Forms.DataVisualization.Charting.GradientStyle.DiagonalRight;
            this.chart1.BorderSkin.BackSecondaryColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.chart1.BorderSkin.BorderColor = System.Drawing.Color.LightSteelBlue;
            this.chart1.BorderSkin.PageColor = System.Drawing.Color.AliceBlue;
            this.chart1.BorderSkin.SkinStyle = System.Windows.Forms.DataVisualization.Charting.BorderSkinStyle.Sunken;
            chartArea1.Area3DStyle.Enable3D = true;
            chartArea1.Area3DStyle.PointDepth = 40;
            chartArea1.Area3DStyle.WallWidth = 5;
            chartArea1.AxisX.ArrowStyle = System.Windows.Forms.DataVisualization.Charting.AxisArrowStyle.Triangle;
            chartArea1.AxisX.IntervalAutoMode = System.Windows.Forms.DataVisualization.Charting.IntervalAutoMode.VariableCount;
            chartArea1.AxisX.MajorGrid.Enabled = false;
            chartArea1.AxisX.MajorTickMark.Enabled = false;
            chartArea1.AxisX.Title = "Grades";
            chartArea1.AxisX.TitleFont = new System.Drawing.Font("Candara", 9F);
            chartArea1.AxisY.ArrowStyle = System.Windows.Forms.DataVisualization.Charting.AxisArrowStyle.Triangle;
            chartArea1.AxisY.Title = "Number of students";
            chartArea1.AxisY.TitleFont = new System.Drawing.Font("Candara", 9F);
            chartArea1.BackColor = System.Drawing.Color.Thistle;
            chartArea1.BackGradientStyle = System.Windows.Forms.DataVisualization.Charting.GradientStyle.DiagonalRight;
            chartArea1.BackSecondaryColor = System.Drawing.Color.MistyRose;
            chartArea1.BorderColor = System.Drawing.Color.AliceBlue;
            chartArea1.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea1);
            legend1.BackColor = System.Drawing.Color.Transparent;
            legend1.Enabled = false;
            legend1.LegendItemOrder = System.Windows.Forms.DataVisualization.Charting.LegendItemOrder.SameAsSeriesOrder;
            legend1.LegendStyle = System.Windows.Forms.DataVisualization.Charting.LegendStyle.Column;
            legend1.Name = "Legend1";
            this.chart1.Legends.Add(legend1);
            this.chart1.Location = new System.Drawing.Point(12, 55);
            this.chart1.Name = "chart1";
            this.chart1.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.None;
            this.chart1.PaletteCustomColors = new System.Drawing.Color[] {
        System.Drawing.Color.Gray,
        System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0))))),
        System.Drawing.Color.Gold,
        System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192))))),
        System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0))))),
        System.Drawing.Color.Olive,
        System.Drawing.Color.PaleVioletRed,
        System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192))))),
        System.Drawing.Color.Red,
        System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0))))),
        System.Drawing.Color.SteelBlue,
        System.Drawing.Color.Purple,
        System.Drawing.Color.Chartreuse,
        System.Drawing.Color.DarkGoldenrod,
        System.Drawing.Color.FloralWhite};
            series1.ChartArea = "ChartArea1";
            series1.CustomProperties = "DrawingStyle=Cylinder";
            series1.Font = new System.Drawing.Font("Candara", 12F);
            series1.IsValueShownAsLabel = true;
            series1.Legend = "Legend1";
            series1.LegendToolTip = "Student Grades";
            series1.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Diamond;
            series1.Name = "Grades";
            series1.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.BrightPastel;
            dataPoint1.AxisLabel = "A";
            dataPoint1.Label = "";
            dataPoint1.LabelToolTip = "Number of students graded A";
            dataPoint1.LegendText = "A(s)";
            dataPoint1.LegendToolTip = "Number of student graded A";
            dataPoint1.ToolTip = "Number of student graded A";
            dataPoint2.AxisLabel = "B";
            dataPoint2.Label = "";
            dataPoint2.LabelToolTip = "Number of students graded B";
            dataPoint2.LegendText = "B(s)";
            dataPoint2.LegendToolTip = "Number of students graded B";
            dataPoint2.ToolTip = "Number of students graded B";
            dataPoint3.AxisLabel = "C";
            dataPoint3.Label = "";
            dataPoint3.LabelToolTip = "Number of students graded C";
            dataPoint3.LegendText = "C(s)";
            dataPoint3.LegendToolTip = "Number of students graded C";
            dataPoint3.ToolTip = "Number of students graded C";
            dataPoint4.AxisLabel = "D";
            dataPoint4.LabelToolTip = "Number of students graded D";
            dataPoint4.LegendText = "D(s)";
            dataPoint4.LegendToolTip = "Number of students graded D";
            dataPoint4.ToolTip = "Number of students graded D";
            dataPoint5.AxisLabel = "E";
            dataPoint5.Label = "";
            dataPoint5.LabelToolTip = "Number of students graded E";
            dataPoint5.LegendText = "E(s)";
            dataPoint5.LegendToolTip = "Number of students graded E";
            dataPoint5.ToolTip = "Number of students graded E";
            dataPoint6.AxisLabel = "F";
            dataPoint6.LegendText = "F(s)";
            dataPoint6.LegendToolTip = "Number of students graded F";
            dataPoint6.ToolTip = "Number of students graded F";
            dataPoint7.AxisLabel = "Missing";
            dataPoint7.LabelToolTip = "Number of students with missing results";
            dataPoint7.LegendToolTip = "Students with missing results";
            dataPoint7.ToolTip = "Students with missing results";
            series1.Points.Add(dataPoint1);
            series1.Points.Add(dataPoint2);
            series1.Points.Add(dataPoint3);
            series1.Points.Add(dataPoint4);
            series1.Points.Add(dataPoint5);
            series1.Points.Add(dataPoint6);
            series1.Points.Add(dataPoint7);
            series1.ToolTip = "Student Grades";
            this.chart1.Series.Add(series1);
            this.chart1.Size = new System.Drawing.Size(608, 278);
            this.chart1.TabIndex = 8;
            this.chart1.Text = "Student\'s Performance Chart";
            this.chart1.Visible = false;
            // 
            // homeFaculty
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(654, 562);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(670, 600);
            this.Name = "HomeFaculty";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Faculty Administration Panel";
            this.panel2.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panelSmart.ResumeLayout(false);
            this.panelSmart.PerformLayout();
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel2.PerformLayout();
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picRating)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolBtnPerformance;
        private System.Windows.Forms.Panel panel3;
        private RibbonMenuButton btnChat;
        private RibbonMenuButton btnApprove;
        private RibbonMenuButton btnPerformance;
        private System.Windows.Forms.ToolStripTextBox searchText;
        private System.Windows.Forms.ToolStripButton toolBtnFind;
        private RibbonMenuButton btnReject;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboDept;
        private System.Windows.Forms.FlowLayoutPanel panelSmart;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboLevel;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox picRating;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox comboCourse;
        private System.Windows.Forms.DataGridViewTextBoxColumn RegNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn dept;
        private System.Windows.Forms.DataGridViewTextBoxColumn Session;
        private System.Windows.Forms.DataGridViewTextBoxColumn Grade;
        private System.Windows.Forms.DataGridViewTextBoxColumn Score;
        private System.Windows.Forms.DataGridViewTextBoxColumn Phone;
        private System.Windows.Forms.ToolStripButton toolBtnChat;
    }
}

