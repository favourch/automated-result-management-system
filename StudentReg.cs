using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace ARMS
{
    public partial class UpdateStudent : Form
    {
        private string Course;

        public UpdateStudent()
        {
            InitializeComponent();
        }

        public UpdateStudent(string course)
        {
            InitializeComponent();
            Course = course;
            var rss = Helper.selectResults(new[] { "regNo"}, "student_courses", new[] { "CourseCode" }, new[] { course }, "");
            foreach (var profile in rss)
            {
                var student = Helper.selectResults(new[] { "email", "dept", "level", "session", "semester", "phone" }, "students", new[] { "regNo" }, new[] { profile[0] }, "")[0];
                int level = Convert.ToInt32(student[2]) * 100;
                var x = new[] { profile[0], student[0], student[1], level.ToString(), student[3], student[4], student[5] };
                regNos.Add(profile[0]);
                rows.Add(x);
                studentsData.Rows.Add(x);
            }
        }

        private List<string[]> rows = new List<string[]>();
        private List<string> regNos = new List<string>();

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (rows.Count > 0 && MessageBox.Show("Are you sure you want to save the filled vales?", "Save students data", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
            {
                foreach (var v in rows)
                {
                    Helper.dbInsert("students", new string[] { "regNo",
                "fName", "lName", "mName", "email", "faculty", "dept", "level", "session", "semester", "degree", "gender" }, v);
                    Helper.dbInsert("student_courses", new string[] { "regNo", "Course Code" }, new[] { v[0], Course });
                }
            }
        }

        private string[] rowToString(DataGridViewCellCollection d)
        {
            string[] thearr = new string[d.Count];
            for (int i = 0; i < d.Count; i++)
            {
                thearr[i] = d[i].EditedFormattedValue.ToString();
            }
            return thearr;
        }

        private void studentsData_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            var r = rowToString(studentsData.Rows[e.RowIndex].Cells);
            if (regNos.Contains(r[0]))
            {
                int i = regNos.IndexOf(r[0]);
                rows.RemoveAt(i);
            }
        }

        private void beginMsgTimer()
        {
            if (msgTimer == null)
            {
                msgTimer = new Timer() { Interval = 500 };
                msgTimer.Tick += msgTimer_Tick;
            }
            msgTimer.Enabled = true;
        }

        private void stopMsgTimer()
        {
            if (msgTimer == null) { return; }
            msgTimer.Enabled = false;
            labelMSG.Visible = false;
        }

        private int count;

        void msgTimer_Tick(object sender, EventArgs e)
        {
            if (count == 5) { stopMsgTimer(); return; }
            count++;
            if (labelMSG.Visible)
            { labelMSG.Visible = false; }
            else { labelMSG.Visible = true; }
        }

        private Timer msgTimer;

        private void studentsData_RowLeave(object sender, DataGridViewCellEventArgs e)
        {
            var d = studentsData.Rows[e.RowIndex].Cells;
            string[] r = new string[d.Count];
            foreach (DataGridViewCell x in d)
            {
                int n = 0;
                if (x.EditedFormattedValue.ToString() == "")
                {
                    labelMSG.Text = "Some values are missing. Please correct these values and try again.";
                    beginMsgTimer();
                    return;
                }
                else
                {
                    r[n] = d[n].EditedFormattedValue.ToString();
                    n++;
                }

            }
            if (regNos.Contains(r[0]))
            {
                int i = regNos.IndexOf(r[0]);
                rows.RemoveAt(i);
                rows.Insert(i, r);
            }
            else
            {
                rows.Add(r);
            }
        }
    }
}
