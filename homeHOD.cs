using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Windows.Forms;
using ARMS.Properties;

namespace ARMS
{
    public partial class homeHOD : Form
    {
        #region Private Variables Declaration
        private readonly List<Activity> acts = new List<Activity>();
        private readonly List<string> addedCourses = new List<string>();
        private readonly Dictionary<string, LecturerDetails> allLecturers = new Dictionary<string, LecturerDetails>();
        private readonly ArrayList allStaff = new ArrayList();
        private readonly Dictionary<string, string> changedCodes = new Dictionary<string, string>();
        private readonly Dictionary<string, Course> changedCourses = new Dictionary<string, Course>();
        private readonly Dictionary<string, LecturerDetails> changedStaff = new Dictionary<string, LecturerDetails>();
        private readonly Dictionary<string, Course> courses = new Dictionary<string, Course>();
        private readonly DeptDetails dd;
        private readonly string username;
        private int Acount;
        private int Bcount;
        private int Ccount;
        private int Dcount;
        private int Ecount;
        private int Fcount;
        private int MRcount;
        private int NRcount;
        private int announceIndex;
        protected List<string> announcements = new List<string>();
        private int cIndex;
        private LecturerDetails currentLecturer;
        private string lastCourseCell;
        private string lastStaffCell;
        private SoundPlayer sp;

        /// <summary>
        /// The amount of time to wait before session is locked. Since timer runs every 3 seconds, this means 300 seconds or three minutes
        /// </summary>
        private int timerTime = 60;
        private int timerCount;

        private int studentTotalScores;
        #endregion

        public homeHOD(string usr, string formalName, DeptDetails dd, bool local)
        {
            this.dd = dd;
            username = usr;
            InitializeComponent();
            labelWelcome.Text = string.Format("Good day {0}.", formalName);
            this.MouseMove += app_MouseMove;
        }

        void app_MouseMove(object sender, MouseEventArgs e)
        {
            timerCount = 0;
        }

        public int ARcount
        {
            get { return MRcount; }
            set { MRcount = value; }
        }

        private void buttonSaveStaff_Click(object sender, EventArgs e)
        {
            var doneKeys = new List<string>();
            foreach (var staff in changedStaff)
            {
                string query = "UPDATE lecturers SET ";

                query += staff.Value.FirstName == null ? "" : string.Format("firstName='{0}'", staff.Value.FirstName);
                query += staff.Value.LastName == null ? "" : string.Format(", lastName='{0}'", staff.Value.LastName);
                query += staff.Value.Email == null ? "" : string.Format(", email='{0}'", staff.Value.Email);
                if (query == "UPDATE lecturers SET ") return;
                query += string.Format(" WHERE staffCode='{0}'", staff.Key);
                int result = Helper.ExecuteNonQuery(query);
                if (result == -1)
                {
                    while (result == -1)
                    {
                        DialogResult dr = MessageBox.Show("The operation has failed. Do you want to try again?",
                                                          staff.Key + " Edit Failed",
                                                          MessageBoxButtons.RetryCancel);
                        if (dr == DialogResult.Cancel) break;
                        result = Helper.ExecuteNonQuery(query);
                    }
                }
                else
                {
                    Helper.logActivity(new Activity
                        {
                            Text = string.Format("The HOD changed the details for {0} {1} and {2}.",
                                                 staff.Value.FirstName == null
                                                     ? ""
                                                     : string.Format("First Name (New value: {0}), ",
                                                                     staff.Value.FirstName),
                                                 staff.Value.LastName == null
                                                     ? ""
                                                     : string.Format("Last Name (New value: {0}), ",
                                                                     staff.Value.LastName),
                                                 staff.Value.Email == null
                                                     ? ""
                                                     : string.Format("Email (New value: {0}), ", staff.Value.Email),
                                                 staff.Key),
                            Department = dd.DeptName,
                            Faculty = dd.Faculty,
                            Username = username
                        });
                    doneKeys.Add(staff.Key);
                }
            }
            foreach (DataGridViewRow row in dataGridViewStaff.Rows)
            {
                if (doneKeys.Contains(row.Cells["CourseCode"].Value.ToString()))
                {
                    row.DefaultCellStyle.ForeColor = Color.Black;
                }
            }
            foreach (string s in doneKeys)
            {
                changedCourses.Remove(s);
            }

            if (changedCourses.Count == 0)
            {
                buttonSaveCourses.Enabled = false;
                MessageBox.Show("All relevant changes has been made successfully.", "Course Edit Successful",
                                MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show(
                    "Relevant changes has been made successfully. However some of the courses were not updated successfully. You can try to update these ones again by clicking Save.",
                    "Course Edit Complete", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void comboBoxCourse_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Clear and Load Main department
            checkedListDepartments.ClearItems();
            checkedListDepartments.AddItem(dd.DeptName);

            //Load other Departments borrowing this course
            foreach (
                var s in
                    Helper.selectResults(new[] {"department"}, "borrowed_courses", new[] {"courseCode"},
                                         new[] {comboBoxCourse.Text}, "")
                )
            {
                checkedListDepartments.AddItem(s[0]);
            }
        }


        private void homeHOD_Load(object sender, EventArgs e)
        {
            tt = new Timer{Interval = 100, Enabled = true};
            tt.Tick += LoadForm;
        }

        private Timer tt;

        private void LoadForm(object state, EventArgs eventArgs)
        {
            tt.Enabled = false;
            textBoxSession.Text = toolTextAcademicYear.Text = Helper.AcademicYear;
            //Load Courses
            string[][] results = Helper.selectResults(new[] { "CourseCode", "CourseTitle", "units", "semester" },
                                          "courses", new[] { "department" }, new[] { dd.DeptName }, "");
            foreach (var s in results)
            {
                try
                {
                    comboBoxCourse.Items.Add(s[0]);
                    comboCourse.Items.Add(s[0]);
                    toolStripComboCourse.Items.Add(s[0]);
                    dataGridViewCourses.Rows.Add(s);
                    courses.Add(s[0],
                                new Course
                                {
                                    CourseCode = s[0],
                                    CourseTitle = s[1],
                                    Units = Convert.ToUInt16(s[2]),
                                    Semester = (Semester)Convert.ToInt32(s[3])
                                });
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(ex.Message + Environment.NewLine + ex.StackTrace,
                                    DateTime.Now.ToShortDateString());
                }
            }

            LoadStaff();
            LoadActivities(false);
            LoadAnnouncements(false);
            timer1.Enabled = true;
        }

        private void LoadStaff()
        {
            dataGridViewStaff.Rows.Clear();
            allLecturers.Clear();
            //Load Lecturers
            string[][] lecturers =
                Helper.selectResults(
                    new[] {"lecturers.username as username", "title", "lastName", "firstName", "email", "staffCode"},
                    "lecturers",
                    new[] {"department", "deleted"}, new[] {dd.DeptName, "N"},
                    "JOIN users ON lecturers.username = users.username");
            int count = 0;
            foreach (var s in lecturers)
            {
                count++;
                var ld = new LecturerDetails
                    {
                        Department = dd.DeptName,
                        Faculty = dd.Faculty,
                        UserName = s[0],
                        Title = s[1],
                        LastName = s[2],
                        FirstName = s[3],
                        Email = s[4],
                        StaffCode = s[5]
                    };
                string lname = string.Format("{0} {1} {2}", ld.Title, ld.LastName, ld.FirstName);
                allLecturers.Add(ld.StaffCode, ld);
                allStaff.Add(new Staff(ld.StaffCode, ld));
                dataGridViewStaff.Rows.Add(new object[] {count, ld.LastName, ld.FirstName, ld.Email, ld.StaffCode});
            }
            comboBoxLecturer1.DisplayMember =
                comboBoxDeptAdmin.DisplayMember = comboBoxExamOfficer.DisplayMember = "Name";
            comboBoxLecturer1.ValueMember =
                comboBoxDeptAdmin.ValueMember = comboBoxExamOfficer.ValueMember = "StaffCode";
            comboBoxLecturer1.DataSource = comboBoxDeptAdmin.DataSource = comboBoxExamOfficer.DataSource = allStaff;
        }

        private void LoadActivities(bool notify)
        {
            foreach (Activity ac in Helper.GetActivities("faculty", dd.Faculty))
            {
                if (ac.IsIn(acts))
                {
                    continue;
                }
                acts.Add(ac);
                dataGridActivity.Rows.Add(new[] {ac.ActivityDate.ToLongDateString(), ac.Text});
                if (notify)
                {
                    notifyIcon1.ShowBalloonTip(3000, "New Activity", ac.Text, ToolTipIcon.Info);
                }
            }
        }

        private void LoadAnnouncements(bool notify)
        {
            foreach (string an in Helper.GetAnnouncements("department", dd.DeptName, UserRoles.HOD))
            {
                if (announcements.Contains(an))
                {
                    continue;
                }
                announcements.Add(an);
                if (notify)
                {
                    notifyIcon1.ShowBalloonTip(3000, "New Annoucement Received", an.StripRTF().Excerpt(40), ToolTipIcon.Info);
                }
            }
            if (announcements.Count == 0)
            {
                buttonPreviousAnnounce.Enabled = false;
            }
            else
            {
                announceIndex = announcements.Count - 1;
                try
                {
                    richTextBoxAnnouncements.Rtf = announcements[announceIndex];
                }
                catch (ArgumentException)
                {
                    richTextBoxAnnouncements.Text = announcements[announceIndex].StripRTF();
                }
                buttonPreviousAnnounce.Enabled = announcements.Count > 1;
            }
            buttonNextAnnounce.Enabled = false;
        }

        private void buttonAssignExams_Click(object sender, EventArgs e)
        {
            assignRole(UserRoles.ExamOfficer);
        }

        private void assignRole(string role)
        {
            string[][] results = Helper.selectResults(new[] {"role"}, "roles",
                                                      new[] {"username", "role"},
                                                      new[] {currentLecturer.UserName, role}, "");
            //Did an error occur?
            if (results == null)
            {
                MessageBox.Show("An error occured while trying to assign this role. Please try again.");
                return;
            }
            if (results.Length > 0)
            {
                DialogResult dr =
                    MessageBox.Show(
                        string.Format(
                            "The {1} role is already assigned to {0}. If you want to unassign this role, Click Retry, otherwise Click Abort/Ignore.",
                            currentLecturer.FormalName, role), "Role Already Assigned",
                        MessageBoxButtons.AbortRetryIgnore,
                        MessageBoxIcon.Warning);
                if (dr == DialogResult.Retry)
                {
                    int d = Helper.DbDelete("roles", new[] {"username", "role"},
                                            new[] {currentLecturer.UserName, role});
                    if (d == -1)
                    {
                        MessageBox.Show("An error occured while trying to unassign this role. Please try again.");
                    }
                    else
                    {
                        Helper.logActivity(
                            new Activity
                                {
                                    Department = dd.DeptName,
                                    Faculty = dd.Faculty,
                                    Username = username,
                                    Text =
                                        string.Format("The HOD has unassigned {0} as {1} in the Department of {2}.",
                                                      currentLecturer.NiceName, role, dd.DeptName)
                                });
                    }
                }
            }
            else
            {
                int di = Helper.dbInsert("roles", new[] {"username", "role"},
                                         new[] {currentLecturer.UserName, role});
                if (di == -1)
                {
                    MessageBox.Show("An error occured while trying to assign this role. Please try again.");
                }
                else
                {
                    Helper.logActivity(
                        new Activity
                            {
                                Department = dd.DeptName,
                                Faculty = dd.Faculty,
                                Username = username,
                                Text =
                                    string.Format("The HOD has assigned {0} as {1} in the Department of {2}.",
                                                  currentLecturer.NiceName, role, dd.DeptName)
                            });

                    MessageBox.Show(
                        string.Format(
                            "The {1} role has been assigned successfully to {0}.",
                            currentLecturer.FormalName, role), "Role Assigned Successfully", MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                }
            }
        }

        private void buttonAssignAdmin_Click(object sender, EventArgs e)
        {
            assignRole("Department Admin");
        }

        private void buttonAssignCourse_Click(object sender, EventArgs e)
        {
            if (toolTextAcademicYear.Text == "" || toolTextAcademicYear.Text.Length < 9)
            {
                var dr = MessageBox.Show(
                    string.Format("The current academic year is missing. Do you want to automatically assume {0} as the academic year?", Helper.AcademicYear),
                    "Academic Year Missing", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                if (dr == DialogResult.OK)
                {
                    toolTextAcademicYear.Text = Helper.AcademicYear;
                }
            }
            try
            {
                //Check if course is already assigned to another lecturer
                string[][] results = Helper.selectResults(new[] {"username", "CourseCode", "department"},
                                                          "lecturer_courses",
                                                          new[] {"department", "CourseCode", "academicYear"},
                                                          new[]
                                                              {
                                                                  dd.DeptName, comboBoxCourse.Text,
                                                                  toolTextAcademicYear.Text
                                                              }, "");
                //Did an error occur?
                if (results == null)
                {
                    MessageBox.Show("An error occured while trying to assign the course. Please try again.");
                    return;
                }
                if (results.Length > 0 && results[0][0] != currentLecturer.UserName)
                {
                    DialogResult cdr = MessageBox.Show(
                        string.Format(
                            "This course is already assinged to {0}. Do you want to unassign this course from {0} and reassign to the currently selected lecturer?",
                            results[0][0]),
                        "Course Already Assigned", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Asterisk);
                    if (cdr == DialogResult.Yes)
                    {
                        //Check if course is already assigned to another lecturer
                        int der = Helper.DbDelete("lecturer_courses", new[] {"username", "CourseCode", "academicYear"},
                                                  new[] {results[0][0], comboBoxCourse.Text, toolTextAcademicYear.Text});
                        if (der == -1)
                        {
                            MessageBox.Show("An error occured while trying to assign the course. Please try again.");
                            return;
                        }
                        Helper.logActivity(
                            new Activity
                                {
                                    Department = dd.DeptName,
                                    Faculty = dd.Faculty,
                                    Username = username,
                                    Text =
                                        string.Format(
                                            "The HOD has unassigned {0} as a lecturer of {1} in the Department of {2}.",
                                            currentLecturer.NiceName, comboBoxCourse.Text, dd.DeptName)
                                });
                    }
                    else return;
                }
                string departmentsError = String.Empty;
                List<string> assDepts = results.Select(n => n[2]).ToList();
                string[] checks = checkedListDepartments.GetChecked();
                if (checks.Length == 0)
                {
                    MessageBox.Show("No department selected. Please select a department and try again.",
                                    "No Department Selected", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                foreach (string s in checks)
                {
                    if (assDepts.Contains(s))
                    {
                        DialogResult dr =
                            MessageBox.Show(
                                string.Format(
                                    "This course is already assigned to {0}. Do you want to unassign this course?",
                                    comboBoxLecturer1.Text), "Course Already Assigned", MessageBoxButtons.YesNo,
                                MessageBoxIcon.Question);
                        if (dr == DialogResult.Yes)
                        {
                            int result = Helper.DbDelete("lecturer_courses",
                                                         new[] {"username", "CourseCode", "academicYear"},
                                                         new[]
                                                             {
                                                                 currentLecturer.UserName, comboBoxCourse.Text,
                                                                 toolTextAcademicYear.Text
                                                             });
                            if (result != -1)
                            {
                                Helper.logActivity(
                                    new Activity
                                        {
                                            Department = dd.DeptName,
                                            Faculty = dd.Faculty,
                                            Username = username,
                                            Text =
                                                string.Format(
                                                    "The HOD has unassigned {0} as a lecturer of {1} in the Department of {2}.",
                                                    currentLecturer.NiceName, comboBoxCourse.Text, dd.DeptName)
                                        });
                            }
                            departmentsError += result == -1 ? s + "\n" : "";
                        }
                        continue;
                    }

                    int di = Helper.dbInsert("lecturer_courses",
                                             new[] {"username", "CourseCode", "department", "academicYear"},
                                             new[]
                                                 {
                                                     currentLecturer.UserName, comboBoxCourse.Text,
                                                     s, toolTextAcademicYear.Text
                                                 });
                    if (di != -1)
                    {
                        Helper.logActivity(
                            new Activity
                                {
                                    Department = dd.DeptName,
                                    Faculty = dd.Faculty,
                                    Username = username,
                                    Text =
                                        string.Format(
                                            "{0} is now a lecturer of {1} in the Department of {2}.",
                                            currentLecturer.NiceName, comboBoxCourse.Text, dd.DeptName)
                                });
                    }

                    departmentsError += di == -1 ? s + "\n" : "";
                }
                if (departmentsError == "")
                {
                    MessageBox.Show("The course was assigned to the specified lecturer successfully.",
                                    "Automated Result Management System",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show(
                        string.Format(
                            "There was an error assigning {0} to {1} for the following departments: {2}. \nPlease try again.",
                            comboBoxCourse.Text,
                            comboBoxLecturer1.Text, departmentsError), "Automated Result Management System",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                }
            }
            catch (Exception)
            {
                MessageBox.Show(
                    string.Format(
                        "There was an error assigning {0} to {1}. Please contact the admin if this continues.",
                        comboBoxCourse.Text, comboBoxLecturer1.Text), "Automated Result Management System",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            if (announcements.Count != Helper.GetAnnouncementCount("department", dd.DeptName, UserRoles.HOD))
            {
                LoadAnnouncements(true);
            }
            if (acts.Count != Helper.GetActivitiesCount("faculty", dd.Faculty))
            {
                LoadActivities(true);
            }
            timerCount++;
            if (timerCount == timerTime)
            {
                toolButtonLock.PerformClick();
            }
            timer1.Enabled = true;
        }

        private void buttonAddCourse_Click(object sender, EventArgs e)
        {
            try
            {
                dataGridViewCourses.Rows.Add();
                DataGridViewCell c = dataGridViewCourses.Rows[dataGridViewCourses.RowCount - 1].Cells[0];
                dataGridViewCourses.CurrentCell = c;
                dataGridViewCourses.BeginEdit(false);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void buttonEditCourse_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection selectedRows = dataGridViewCourses.SelectedRows;
            if (selectedRows.Count == 0) return;
            DataGridViewCell cell = selectedRows[0].Cells[0];
            dataGridViewCourses.CurrentCell = cell;
            dataGridViewCourses.BeginEdit(false);
        }

        private void buttonDeleteCourse_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection selectedRows = dataGridViewCourses.SelectedRows;
            if (selectedRows.Count == 0) return;
            DataGridViewCell cell = selectedRows[0].Cells[0];
            string course = dataGridViewCourses.CurrentRow.Cells["CourseCode"].Value.ToString();
            if (addedCourses.Contains(course))
            {
                dataGridViewCourses.Rows.RemoveAt(cell.RowIndex);
                addedCourses.Remove(course);
                if (changedCourses.ContainsKey(course))
                {
                    changedCourses.Remove(course);
                }
                if (changedCodes.ContainsKey(course))
                {
                    changedCodes.Remove(course);
                }
                return;
            }
            if (Helper.DbDelete("courses", "CourseCode", course) > 0)
            {
                MessageBox.Show("The selected course has been delete successfully.", course + " Deleted.",
                                MessageBoxButtons.OK, MessageBoxIcon.Information);
                dataGridViewCourses.Rows.RemoveAt(cell.RowIndex);
                Helper.logActivity(new Activity
                    {
                        Text =
                            string.Format("The COURSE {0} has been removed from the department of {1}", course,
                                          dd.DeptName),
                        Faculty = dd.Faculty,
                        Department = dd.DeptName,
                        Username = username
                    });
            }
            else
            {
                MessageBox.Show("An error occurred while trying to delete the specified course. Please try again.",
                                "Error Deleting Course", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void buttonAddStaff_Click(object sender, EventArgs e)
        {
            //Let's generate a random staff code
            var rnd = new Random(DateTime.Now.Millisecond);

            //Let's retrieve a list of current staff codes
            List<string> staffcodes = allLecturers.Select(n => n.Value.StaffCode).ToList();
            string staffCode = dd.ShortName + rnd.Next(1000, 9999);
            while (staffcodes.Contains(staffCode))
            {
                staffCode = dd.ShortName + rnd.Next(1000, 9999);
            }

            var lr = new LecturerReg(username, dd.DeptName, dd.Faculty, staffCode);
            lr.ShowDialog();
            LoadStaff();
        }

        private void buttonEditStaff_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection selectedRows = dataGridViewStaff.SelectedRows;
            if (selectedRows.Count == 0)
            {
                MessageBox.Show("No staff selected. Select a staff to edit from the list first and try again.", "No Staff Selected", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                return;
            }
            DataGridViewCell cell = selectedRows[0].Cells["StaffCode"];
            var lec = allLecturers[cell.Value.ToString()];
            LecturerReg lr = new LecturerReg(lec.UserName, false, lec, true);
            lr.ShowDialog();
/*
            dataGridViewStaff.CurrentCell = cell;
            dataGridViewStaff.BeginEdit(false);
*/
        }

        private void buttonDeleteStaff_Click(object sender, EventArgs e)
        {
            DataGridViewRow curRow = dataGridViewStaff.SelectedRows[0];
            if (curRow != null)
            {
                DialogResult dr = MessageBox.Show(
                    "Are you sure you want to remove this staff? This user will no longer be able to login to the system and any existing records on the system will be archived automatically. Press yes to continue and remove this staff or no to cancel this action.",
                    "Delete Staff", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (dr == DialogResult.Yes)
                {
                    LecturerDetails lecturer = allLecturers[curRow.Cells["staffCode"].Value.ToString()];
                    string usrName = lecturer.UserName;
                    int result = Helper.Update("users", new[] {"deleted"}, new[] {"Y"}, new[] {"username"},
                                               new[] {usrName}, "");
                    if (result == 1)
                    {
                        MessageBox.Show("The selected staff has been deleted successfully.",
                                        "Staff Removed Successfully", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        var act = new Activity
                            {
                                Department = dd.DeptName,
                                Faculty = dd.Faculty,
                                Username = username,
                                Text =
                                    string.Format("The HOD has Removed Staff {0} from the department of {1}.",
                                                  lecturer.FormalName, lecturer.Department)
                            };
                        Helper.logActivity(act);
                        dataGridViewStaff.Rows.Remove(curRow);
                        LoadStaff();
                    }
                    else
                    {
                        MessageBox.Show("An error occurred while trying to remove the specified staff. Please try again");
                    }
                }
            }
        }

        private void dataGridViewStaff_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow nRow = dataGridViewStaff.Rows[e.RowIndex];
            cIndex = e.ColumnIndex;
            try
            {
                if (nRow.Cells[cIndex].Value.ToString() != lastStaffCell)
                {
                    string key = nRow.Cells["staffCode"].Value.ToString();
                    string cname = dataGridViewCourses.Columns[cIndex].Name;
                    if (!changedCourses.ContainsKey(key))
                    {
                        changedStaff.Add(key, new LecturerDetails {StaffCode = key});
                    }
                    LecturerDetails staff = changedStaff[key];
                    switch (cname)
                    {
                        case "FirstName":
                            staff.FirstName = nRow.Cells["FirstName"].Value.ToString();
                            changedCodes.Add(lastCourseCell, key);
                            break;
                        case "LastName":
                            staff.LastName = nRow.Cells["LastName"].Value.ToString();
                            break;
                        case "Email":
                            staff.Email = nRow.Cells["email"].Value.ToString();
                            break;
                    }
                    nRow.DefaultCellStyle.ForeColor = Color.Red;
                    buttonSaveCourses.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                dataGridViewStaff.CancelEdit();
                Logger.WriteLog(ex.Source + ex.Message, ex.StackTrace);
                MessageBox.Show(Resources.staffIncorrectInput, "Incorrect Input",
                                MessageBoxButtons.OK);
            }
        }

        private void dataGridViewStaff_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            try
            {
                lastStaffCell = dataGridViewStaff.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();
            }
            catch
            {
            }
        }

        private void btnUpdateData_Click(object sender, EventArgs e)
        {
            try
            {
                string programmes =
                    Helper.selectResults(new[] {"group_concat(programme)"}, "programmes", new[] {"deptName"},
                        new[] {dd.DeptName}, "")[0][0];
                dd.Programmes = programmes.Split(',');
            }
            catch (Exception ex)
            {
                var dr = MessageBox.Show(
                    "An error occurred while trying to retrieve the department details. Do you want to try again?",
                    "Error Checking Department", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dr == System.Windows.Forms.DialogResult.No)
                {
                    return;
                }
            }
            var def = new DepartmentEditForm(dd, username);
            def.ShowDialog();
        }

        private void dataGridViewCourses_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            try
            {
                lastCourseCell = dataGridViewCourses.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();
            }
            catch
            {
            }
        }

        //old value, new value

        private void dataGridViewCourses_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow nRow = dataGridViewCourses.Rows[e.RowIndex];
            cIndex = e.ColumnIndex;
            try
            {
                if (nRow.Cells[cIndex].Value.ToString() != lastCourseCell)
                {
                    string key = nRow.Cells["CourseCode"].Value.ToString();
                    string cname = dataGridViewCourses.Columns[cIndex].Name;
                    if (!changedCourses.ContainsKey(key))
                    {
                        if (cname == "CourseCode")
                        {
                            if (!courses.ContainsKey(key))
                            {
                                addedCourses.Add(key);
                                return;
                            }
                            changedCodes.Add(lastCourseCell, key);
                            changedCourses.Add(key, new Course {CourseCode = key});
                            return;
                        }
                        changedCourses.Add(key, new Course {CourseCode = key});
                    }
                    Course cc = changedCourses[key];
                    switch (cname)
                    {
                        case "CourseCode":
                            cc.CourseCode = nRow.Cells["CourseCode"].Value.ToString();
                            changedCodes.Add(lastCourseCell, key);
                            break;
                        case "CourseTitle":
                            cc.CourseTitle = nRow.Cells["CourseTitle"].Value.ToString();
                            break;
                        case "semester":
                            cc.Semester = (Semester) Convert.ToInt32(nRow.Cells["semester"].Value.ToString());
                            break;
                        case "units":
                            cc.Units = Convert.ToUInt16(nRow.Cells["units"].Value.ToString());
                            break;
                    }
                    nRow.DefaultCellStyle.ForeColor = Color.Red;
                    buttonSaveCourses.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                dataGridViewCourses.CancelEdit();
                Logger.WriteLog(ex.Source + ex.Message, ex.StackTrace);
                MessageBox.Show(Resources.staffIncorrectInput, "Incorrect Input", MessageBoxButtons.OK);
            }
        }

        private void comboBoxLecturer1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                var combo = (ComboBox) sender;
                currentLecturer = allLecturers[combo.SelectedValue.ToString()];
            }
            catch (Exception ex)
            {
                Logger.WriteLog(ex);
            }
        }

        private void btnAnnouncements_Click(object sender, EventArgs e)
        {
            var an = new Announcer(username, dd.Faculty, dd.DeptName);
            an.ShowDialog();
        }

        private void clearStudentData()
        {
            studentTotalScores = 0;
            studentsData.Rows.Clear();
            Acount = Bcount = Ccount = Dcount = Ecount = Fcount = MRcount = NRcount = 0;
            labelA.Text = string.Format("As: {0}", Acount);
            labelB.Text = string.Format("Bs: {0}", Bcount);
            labelC.Text = string.Format("Cs: {0}", Ccount);
            labelD.Text = string.Format("Ds: {0}", Dcount);
            labelE.Text = string.Format("Es: {0}", Ecount);
            labelF.Text = string.Format("Fs: {0}", Fcount);
            labelAR.Text = string.Format("Ommitted Results: {0}", MRcount);
            labelNR.Text = string.Format("No Results: {0}", NRcount);
        }

        private void comboCourse_SelectedIndexChanged(object sender, EventArgs e)
        {
            clearStudentData();
            comboBoxCourse.Text = toolStripComboCourse.Text = comboCourse.Text;
            //load all results for selected course for the current academic session
            string[][] results = Helper.selectResults(new[] {"regNo", "CourseCode", "ca_score", "exam_score"},
                                                      "student_exams",
                                                      new[] {"academicYear", "CourseCode"},
                                                      new[] {textBoxSession.Text, comboCourse.Text}, "");
            if (results.Length == 0)
            {
                btnApprove.ColorBase = btnReject.ColorBase = Color.Gainsboro;
                return;
            }
            string orclause = String.Join(" OR regNo = ", results.Select(n => "'" + n[0] + "'").ToArray());
            string[][] sresults =
                Helper.selectResults(
                    new[] {"CONCAT(lName, ' ', fName, ' ', mName) as studentName", "regNo", "dept", "level"},
                    "students", " WHERE regNo = " + orclause);
            int index = 0;
            for (int i = 0; i < results.Length; i++)
            {
                string[] student = results[i];
                if (sresults[i][2] != dd.DeptName)
                {
                    //not needed at department level. Approved at faculty
                    continue;
                }
                index++;
                var sd = new StudentData
                    {
                        StudentName = sresults[i][0],
                        RegNo = sresults[i][1],
                        Dept = sresults[i][2],
                        Level = sresults[i][3]
                    };
                sd.CaMark = student[2];
                sd.ExamMark = student[3];
                switch (sd.TotalMark)
                {
                    case "MR":
                        MRcount++;
                        labelAR.Text = string.Format("Ommitted Results: {0}", MRcount);
                        break;
                    case "NR":
                        NRcount++;
                        labelNR.Text = string.Format("No Results: {0}", NRcount);
                        break;
                }
                var score = (int) sd.TotalScore;
                studentTotalScores += score;
                IncreaseScoreCount(score);
                studentsData.Rows.Add(new[]
                    {index.ToString(), sd.StudentName, sd.RegNo, sd.CaMark, sd.ExamMark, sd.TotalMark, sd.GradeLetter});
                studentsData.Update();
            }
            int prating = studentTotalScores/(studentsData.RowCount);
            labelPerformance.Text = Helper.GetRating(prating);

            string result =
                Helper.selectResults(new[] {"dapproved"}, "lecturer_courses",
                                     new[] {"academicYear", "department", "CourseCode"},
                                     new[] {textBoxSession.Text, dd.DeptName, comboCourse.Text}, "")[0][0];
            labelApproved.Text = result == "Y" ? "Yes" : "No";
            btnApprove.Enabled = btnReject.Enabled = result != "Y";
            if (btnApprove.Enabled)
                btnApprove.ColorBase = btnReject.ColorBase = Color.FromArgb(192, 255, 192);
            else btnApprove.ColorBase = btnReject.ColorBase = Color.Gainsboro;
        }

        private void IncreaseScoreCount(int score)
        {
            if (score >= 70 && score <= 100)
            {
                Acount++;
                labelA.Text = string.Format("As: {0}", Acount);
            }
            else if (score >= 60 && score < 70)
            {
                Bcount++;
                labelB.Text = string.Format("Bs: {0}", Bcount);
            }
            else if (score >= 50 && score < 60)
            {
                Ccount++;
                labelC.Text = string.Format("Cs: {0}", Ccount);
            }
            else if (score >= 45 && score < 50)
            {
                Dcount++;
                labelD.Text = string.Format("Ds: {0}", Dcount);
            }
            else if (score >= 40 && score < 45)
            {
                Ecount++;
                labelE.Text = string.Format("Es: {0}", Ecount);
            }
            else
            {
                Fcount++;
                labelF.Text = string.Format("Fs: {0}", Fcount);
            }
        }

        private void btnApprove_Click(object sender, EventArgs e)
        {
            //Approval Codes here
            DialogResult dr = MessageBox.Show(
                string.Format(
                    "Are you sure you want to approve the results for {0}. Once approved this action is final unless it is rejected by the Faculty. Do you want to continue?",
                    comboCourse.Text), "Confirm Approval", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (dr == DialogResult.Yes)
            {
                ApproveCourse(comboCourse.Text, "dapproved", true);
            }
        }

        private void btnReject_Click(object sender, EventArgs e)
        {
            //Rejection codes here
            DialogResult dr = MessageBox.Show(
                string.Format(
                    "Are you sure you want to reject the results for {0}. Once rejected the lecturer is able to re-submit the results for re-evaluation. Do you want to continue?",
                    comboCourse.Text), "Confirm Disapproval", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (dr == DialogResult.Yes)
            {
                ApproveCourse(comboBoxCourse.Text, "dapproved", false);
            }
        }

        private void ApproveCourse(string CourseCode, string approveLevel, bool approved)
        {
            int update = Helper.Update("lecturer_courses", new[] {approveLevel}, new[] {approved ? "Y" : "N"},
                                       new[] {"academicYear", "department", "CourseCode"},
                                       new[] {textBoxSession.Text, dd.DeptName, CourseCode}, "");
            if (update == 1)
            {
                MessageBox.Show(
                    string.Format("The results for {0} has been {1} by the HOD.", CourseCode,
                                  approved ? "approved." : "rejected."),
                    "Results " + (approved ? "Approved" : "Rejected"), MessageBoxButtons.OK, MessageBoxIcon.Information);

                btnApprove.Enabled = btnReject.Enabled = false;
                btnApprove.ColorBase = btnReject.ColorBase = Color.Gainsboro;

                labelApproved.Text = approved ? "Yes" : "No";

                int logresult =
                    Helper.logActivity(new Activity
                        {
                            Department = dd.DeptName,
                            Faculty = dd.Faculty,
                            Text =
                                string.Format(
                                    "The results for {0} has been {2} by the HOD. Result Rating: {1}.",
                                    CourseCode, labelPerformance.Text, approved ? "approved" : "rejected"),
                            Username = username
                        });
            }
            else
            {
                DialogResult dr =
                    MessageBox.Show(
                        string.Format("An error occurred while {0} the results. Do you want to try again.",
                                      approved ? "approving" : "rejecting"),
                        string.Format("Results {0} Error", approved ? "Approval" : "Rejection"),
                        MessageBoxButtons.RetryCancel, MessageBoxIcon.Error);
                if (dr == DialogResult.Retry)
                {
                    ApproveCourse(CourseCode, approveLevel, approved);
                }
            }
        }

        private void buttonSaveCourses_Click_1(object sender, EventArgs e)
        {
            var doneCodes = new List<string>();
            //Let's add all the courses first.
            foreach (string s in addedCourses)
            {
                if (
                    Helper.dbInsert("courses", new[] {"CourseCode", "department", "faculty"},
                                    new[] {s, dd.DeptName, dd.Faculty},
                                    true) == -1)
                {
                    MessageBox.Show("An error occurred while trying to save the changes.", "Unable to Save all Changes");
                    return;
                }
                Helper.logActivity(new Activity
                    {
                        Department = dd.DeptName,
                        Faculty = dd.Faculty,
                        Username = username,
                        Text = string.Format("The HOD has added a new course for the department of {0}.", dd.DeptName)
                    });
            }
            foreach (var s in changedCodes)
            {
                string query = string.Format("UPDATE course set CourseCode='{1}' WHERE CourseCode='{0}'", s.Key, s.Value);

                int result = Helper.ExecuteNonQuery(query);
                if (result == -1)
                {
                    while (result == -1)
                    {
                        DialogResult dr = MessageBox.Show("The operation has failed. Do you want to try again?",
                                                          s.Key + " Edit Failed", MessageBoxButtons.RetryCancel);
                        if (dr == DialogResult.Cancel) break;
                        result = Helper.ExecuteNonQuery(query);
                    }
                }
                else
                {
                    string log = string.Format(" changed the CourseCode for {0} to {1}", s.Key, s.Value);
                    Helper.logActivity(new Activity
                        {
                            Department = dd.DeptName,
                            Faculty = dd.Faculty,
                            Text = log,
                            Username = username
                        });
                    doneCodes.Add(s.Key);
                }
            }
            foreach (string s in doneCodes)
            {
                changedCodes.Remove(s);
            }
            var doneKeys = new List<string>();
            foreach (var c in changedCourses)
            {
                string query = "UPDATE courses SET ";
                query += c.Value.CourseTitle == null ? "" : string.Format("CourseTitle='{0}'", c.Value.CourseTitle);
                query += c.Value.Semester == 0 ? "" : string.Format(", semester='{0}'", c.Value.Semester);
                query += c.Value.Units == 0 ? "" : string.Format(", units='{0}'", c.Value.Units);
                if (query == "UPDATE courses SET ") return;
                query += string.Format(" WHERE CourseCode='{0}'", c.Key);
                int result = Helper.ExecuteNonQuery(query);
                if (result == -1)
                {
                    while (result == -1)
                    {
                        DialogResult dr = MessageBox.Show("The operation has failed. Do you want to try again?",
                                                          c.Key + " Edit Failed",
                                                          MessageBoxButtons.RetryCancel);
                        if (dr == DialogResult.Cancel) break;
                        result = Helper.ExecuteNonQuery(query);
                    }
                }
                else
                {
                    string log = string.Format(" changed the {0} {1} {2} for {3}",
                                               c.Value.CourseTitle == null
                                                   ? ""
                                                   : string.Format("CourseTitle (New value: {0}), ", c.Value.CourseTitle),
                                               c.Value.Semester == 0
                                                   ? ""
                                                   : string.Format("Semester (New value: {0}), ", c.Value.Semester),
                                               c.Value.Units == 0
                                                   ? ""
                                                   : string.Format("Unit Load (New value: {0}), ", c.Value.Units), c.Key);
                    Helper.logActivity(new Activity
                        {
                            Department = dd.DeptName,
                            Faculty = dd.Faculty,
                            Text = log,
                            Username = username
                        });
                    doneKeys.Add(c.Key);
                }
            }
            foreach (DataGridViewRow row in dataGridViewCourses.Rows)
            {
                if (doneKeys.Contains(row.Cells["CourseCode"].Value.ToString()))
                {
                    row.DefaultCellStyle.ForeColor = Color.Black;
                }
            }
            foreach (string s in doneKeys)
            {
                changedCourses.Remove(s);
            }

            if (changedCourses.Count == 0)
            {
                buttonSaveCourses.Enabled = false;
                MessageBox.Show("All relevant changes has been made successfully.", "Course Edit Successful",
                                MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show(
                    "Relevant changes has been made successfully. However some of the courses were not updated successfully. You can try to update these ones again by clicking Save.",
                    "Course Edit Complete", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void toolButtonLock_Click(object sender, EventArgs e)
        {
            var lc = new Locked(username);
            Visible = false;
            lc.UnlockEvent += (o, args) => { Visible = args.Unlocked; timerCount = 0; };
            lc.ShowDialog();
        }

        private void notifyIcon1_BalloonTipClicked(object sender, EventArgs e)
        {
            tabControl1.SelectTab(0);
            BringToFront();
            Focus();
            if (WindowState == FormWindowState.Minimized)
            {
                WindowState = FormWindowState.Normal;
            }
        }

        private void notifyIcon1_BalloonTipShown(object sender, EventArgs e)
        {
            sp = sp ?? new SoundPlayer(Resources._2cool);
            sp.Play();
        }
    }
}
