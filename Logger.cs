using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace ARMS
{
    public class Logger
    {
        public static void WriteLog(Exception e)
        {
            File.AppendAllText(AppHelper.AppPath + "\\" + "eCampusErrors.log", e.Message + Environment.NewLine + e.StackTrace + Environment.NewLine + DateTime.Now.ToShortDateString() + Environment.NewLine + Environment.NewLine);
        }

        public static void WriteLog(Exception e, bool critical)
        {
            File.AppendAllText(AppHelper.AppPath + "\\" + "eCampusErrors.log", e.Message + Environment.NewLine + e.StackTrace + Environment.NewLine + DateTime.Now.ToShortDateString() + Environment.NewLine + Environment.NewLine);
            if (critical)
            {
                Thread t = new Thread(ThreadLog);
                t.Start(e);
            }
        }

        private static void ThreadLog(object e)
        {
            Exception ex = e as Exception;
            SendLog(ex);
        }

        public static void WriteLog(string source, string data)
        {
            File.AppendAllText(AppHelper.AppPath + "\\" + "eCampusErrors.log", source + Environment.NewLine + data + Environment.NewLine + Environment.NewLine);
            SendLog(source, data);
        }

        public static void SendLog(Exception e)
        {
            try
            {
                Helper.dbInsert("errors", new[] { "message", "stackTrace" },
                         new[] { e.Message, e.StackTrace });
            }
            catch (Exception ex)
            {
                WriteLog(ex, false);
            }
        }

        public static void SendLog(string source, string data)
        {
            //Todo: Send data to Central DB
            try
            {
                Helper.dbInsert("logs", new[] { "username", "action" }, new[] { source, data });
            }
            catch (Exception ex)
            {
                WriteLog(ex);
            }
        }
    }
}
