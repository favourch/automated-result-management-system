using System;
using System.Windows.Forms;

namespace ARMS
{
    /// <summary>
    /// The Message Item control for the Message Display control.
    ///  The MessageItem is the small control that displays the sender and subject of the message
    /// When Clicked, it should show the main message in the Message Display Control
    /// </summary>
    public partial class MessageItem : UserControl
    {
        public Messages message;

        /// <summary>
        /// Default constructore for the MessageItem control
        /// </summary>
        public MessageItem()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initializes the MessageItem control and assigsns a message and display picture to it
        /// </summary>
        /// <param name="msg">The Message object model to use</param>
        /// <param name="picUrl">The url/website to the display picture</param>
        public MessageItem(Messages msg, string picUrl)
        {
            InitializeComponent();
            message = msg;
            pictureBoxUser.ImageLocation = picUrl;
            labelSender.Text = message.Sender;
            labelSubject.Text = message.Subject;
        }

        private void MessageItem_Click(object sender, EventArgs e)
        {
            OnClick(e);
        }

        /// <summary>
        /// Gets or return the current image being displayed in the MessageItem instance.
        /// </summary>
        public System.Drawing.Image Picture
        {
            get { return pictureBoxUser.Image; }
        }
    }
}
