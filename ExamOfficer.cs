using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Media;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using Microsoft.Office.Interop.Excel;
using Application = System.Windows.Forms.Application;

namespace ARMS
{
    public partial class ExamOfficer : Form
    {
        private DeptDetails dd;
        private string username;
        private string tempName;
        private string savedFileName;
        private bool local;

        public ExamOfficer(DeptDetails dd, string formalName, string username, bool local)
        {
            InitializeComponent();
            labelWelcome.Text = string.Format("Good day {0}.", formalName);
            this.local = local;
            labelMode.Text = local ? "Local Mode" : "Online";
            this.dd = dd;
            this.username = username;
            this.MouseMove += app_MouseMove;
        }

        void app_MouseMove(object sender, MouseEventArgs e)
        {
            timerCount = 0;
        }

        private void tabPageStudents_Click(object sender, EventArgs e)
        {

        }

        private void buttonGenerateReports_Click(object sender, EventArgs e)
        {
            if (checkLocal())
            {
                return;
            }
            if (radioBroadSheet.Checked)
            {
                GenerateBroadSheet(GetCheckedLevel());
            }
            else if (radioFacultySummary.Checked)
            {
                GenerateFacultySummary(GetCheckedLevel());
            }
        }

        private void GenerateFacultySummary(string level)
        {
            
        }

        private void GenerateBroadSheet(string level)
        {
            BroadGen bg = new BroadGen();
            tempName = Application.UserAppDataPath + "\\" + level + "broadsheet.xlsx";
            string markit = "✔";
            //if file exists...
            if(File.Exists(tempName))
            {
                DialogResult xd;
                xd = savedFileName == null ? DialogResult.OK : MessageBox.Show("It seems this file has already been generated. Press Cancel to cancel this operation and open the exiting file? or Press OK to overwrite the file.", "File Already Exists", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                if (xd == DialogResult.OK)
                {
                    try
                    {
                        File.Delete(tempName);
                    }
                    catch
                    {
                        foreach (var p in Process.GetProcessesByName("excel"))
                        {
                            p.Kill();
                        }
                        File.Delete(tempName);
                    }
                }
                else
                {
                    File.Move(tempName, savedFileName);
                    viewFile();
                    progressLabel.Text = "BroadSheet Ready.";
                    return;
                }
            }
            string semester = radioSemester1.Checked ? "FIRST" : "SECOND";
            bg.CreatePackage(tempName, dd.DeptName, dd.Faculty, labelCurrentSession.Text, semester, level);
            myProgress.Value = 5;

            //Retrieve all students registration for the current level, semester and session
            progressLabel.Text = "Retrieving Course Registration...";
            string courseQuery =
                string.Format(
                    "SELECT DISTINCT student_courses.regNo, student_courses.CourseCode, courses.units, CONCAT(students.lName, CONCAT(' ', SUBSTR(students.fName,1,1), '. ', SUBSTR(students.mName,1,1)), '.')" +
                    " from student_courses JOIN (students, courses)" +
                    " ON students.regNo=student_courses.regNo AND courses.CourseCode = student_courses.CourseCode" +
                    " WHERE academicYear = '{0}' AND students.dept = '{1}' AND students.level = '{3}' AND courses.semester = '{2}' AND courses.department='{4}' ORDER by student_courses.CourseCode DESC", labelCurrentSession.Text, dd.DeptName, radioSemester1.Checked ? "1" : "2", level, dd.DeptName);
            var registered = Helper.ExecuteSelect(courseQuery);
            myProgress.Value = 8;
            if (registered == null || registered.Length == 0)
            {
                MessageBox.Show(
                    "It seems there are no registered courses for the specified semester/session. Please cross check you have selected the correct values and try again.",
                    "NO Courses Found", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
            else
            {
                Dictionary<string, Course> courses = new Dictionary<string, Course>();
                foreach (var course in registered)
                {
                    if (!courses.ContainsKey(course[1]))
                        courses.Add(course[1], new Course {Units = Convert.ToUInt16(course[2])});
                }

                //Time to retrieve all results
                progressLabel.Text = "Retrieving Results from Database...";
                string examQuery =
                    string.Format(
                        "SELECT DISTINCT student_exams.regno, student_exams.CourseCode, exam_score + ca_score" +
                        " from student_exams JOIN (students, courses)" +
                        " ON students.regNo=student_exams.regNo AND courses.CourseCode = student_exams.CourseCode" +
                        " WHERE academicYear = '{0}' AND students.dept = '{1}' AND students.level = '{3}' AND courses.semester = '{2}' AND courses.department='{4}'",
                        labelCurrentSession.Text, dd.DeptName, radioSemester1.Checked ? "1" : "2", level, dd.DeptName);

                var results = Helper.ExecuteSelect(examQuery);

                myProgress.Value = 10;

                if (results == null || results.Length == 0)
                {
                    MessageBox.Show(
                        "It seems there are no results for the specified semester/session yet. It is most likely no results have yet been submitted for this semester.",
                        "No Results Found", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
                else
                {
                    if ((registered.Length / 2) > results.Length)
                    {
                        var dr = MessageBox.Show(
                            "It seems a lot of results have not be submitted yet. Are you sure you want to continue?",
                            "Many Awaiting Results", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                        if (dr == DialogResult.Cancel) return;
                    }
                    var studentCourses = new Dictionary<string, List<StudentResult>>();
                    double step = 45.0/registered.Length;
                    double progress = myProgress.Value;

                    var examCourses = results.ToDictionary(n => n[0] + ":" + n[1]);

                    //Let's go through the student's registered courses and store results where relevant
                    for (int i = 0; i < registered.Length; i++)
                    {
                        var course = registered[i];

                        //First of all, if the result entry has not been created yet, add it
                        if(!studentCourses.ContainsKey(course[0]))
                                studentCourses.Add(course[0], new List<StudentResult>());
                        string key = course[0] + ":" + course[1];
                        //Then let's check if the course results exists
                        if (examCourses.ContainsKey(key))
                        {
                            studentCourses[course[0]].Add(new StudentResult
                            {
                                CourseCode = course[1],
                                Name = course[3],
                                TotalScore = examCourses[key][2],
                            });
                        }
                        else
                        {
                            //if it doesn't exist, let's create a new result with default values
                            studentCourses[course[0]].Add(new StudentResult
                            {
                                CourseCode = course[1],
                                Name = course[3],
                            });
                        }
                        progress += step;
                        myProgress.Value = Convert.ToInt32(progress);
                    }

                    //Okay, so processing done time to insert the values in excel
                    progressLabel.Text = "Inserting Student Results...";
                    Helper.InsertBroadSheetData(tempName, studentCourses, courses, "Sheet1", 3, 5);
                    progressLabel.Text = "Document Complete. Save now...";
                    myProgress.Value = 95;
                    if (sfd.ShowDialog() == DialogResult.OK)
                    {
                        saveFile(sfd.FileName);
                    }
                    myProgress.Value = 100;
                    progressLabel.Text = "BroadSheet Ready.";
                }
            }
        }

        private void viewFile()
        {

            if (savedFileName == null)
            {
                MessageBox.Show(
                    "You have to save the file first before you can open or view. Press OK to choose a location to save your file.");
            }
            else
            {
                if (File.Exists(savedFileName))
                {
                    Process.Start("excel", savedFileName);
                }
                else
                {
                    MessageBox.Show(
                        "It seems the file you are trying to open has been renamed or does not exist anymore.");
                }
            }
        }

        private void saveFile(string fileName)
        {
            try
            {
                if (File.Exists(fileName))
                {
                    if (tempName == fileName)
                    {
                        return;
                    }
                    if (savedFileName == fileName)
                    {
                        var dd = MessageBox.Show("This file has already been saved. Click Ok to open the file, or Click Cancel to abort.", "File Already Exists", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                        if (dd == System.Windows.Forms.DialogResult.OK)
                        {
                            return;
                        }
                    }
                    else
                    {
                        File.Delete(fileName);
                    }
                }
                if (File.Exists(tempName) ||  savedFileName == null)
                {
                    File.Move(tempName, fileName);
                }
                else
                {
                    File.Move(savedFileName, fileName);
                }
                savedFileName = fileName;
            }
            catch (Exception ex)
            {
                var dr = MessageBox.Show("It seems this file is currently open in Excel. Do you want eCampus to shutdown all open Excel Applications and attempt to save again? You can close the file manually and Click Retry or Click Ignore to automatically close all open apps and continue.", "File Save Error", MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Hand);
                if (dr == System.Windows.Forms.DialogResult.Retry)
                {
                    saveFile(fileName);
                }
                else if (dr == System.Windows.Forms.DialogResult.Ignore)
                {
                    foreach (Process p in Process.GetProcessesByName("excel"))
                    {
                        p.Kill();
                    }
                    saveFile(fileName);
                }
            }
        }

        private string GetCheckedLevel()
        {
            foreach (Control c in groupBoxLevel.Controls)
            {
                RadioButton rb = c as RadioButton;
                if (rb != null && rb.Checked)
                {
                    return rb.Text.Substring(0, 3);
                }
            }
            return String.Empty;
        }

        private void btnAnnouncer_Click(object sender, EventArgs e)
        {
            Announcer an = new Announcer(username, dd.Faculty, dd.DeptName);
            an.ShowDialog();
        }

        private void statusStrip1_SizeChanged(object sender, EventArgs e)
        {
            myProgress.Size = new Size(statusStrip1.Width - 261, myProgress.Height);
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            toolTextAcademicYear.Text = labelCurrentSession.Text = numericUpDownSession.Value + "/" + (numericUpDownSession.Value + 1);
        }

        private void ExamOfficer_Load(object sender, EventArgs e)
        {
            tt = new Timer{Interval = 100, Enabled = true};
            tt.Tick += LoadForm;
        }

        private Timer tt;

        private void LoadForm(object state, EventArgs eventArgs)
        {
            tt.Enabled = false;
            numericUpDownSession.Value = Int32.Parse(Helper.AcademicYear.Substring(0, 4));
            toolComboSemester.Text = Helper.getProperty("semester");
            LoadAnnouncements(false);
            LoadActivities(false);
            timer1.Enabled = true;
        }

        private List<Activity> acts = new List<Activity>();

        private void LoadActivities(bool notify)
        {
            List<Activity> acs = Helper.GetActivities("department", dd.DeptName);
            foreach (var ac in acs)
            {
                if (ac.IsIn(acts))
                {
                    continue;
                }
                acts.Add(ac);
                dataGridActivity.Rows.Add(new[] { ac.ActivityDate.ToShortDateString(), ac.Text });
                if (notify)
                {
                    notifyIcon1.ShowBalloonTip(3000, "New Activity", ac.Text, ToolTipIcon.Info);
                }
            }
        }

        private void LoadAnnouncements(bool notify)
        {
            foreach (var an in Helper.GetAnnouncements("department", dd.DeptName, UserRoles.ExamOfficer))
            {
                if (announcements.Contains(an))
                {
                    continue;
                }
                announcements.Add(an);
                if (notify)
                {
                    notifyIcon1.ShowBalloonTip(3000, "New Annoucement Received", an.StripRTF().Excerpt(40), ToolTipIcon.Info);;
                }
            }
            if (announcements.Count == 0)
            {
                buttonPreviousAnnounce.Enabled = false;
            }
            else
            {
                announceIndex = announcements.Count - 1;
                try
                {
                    richTextBoxAnnouncements.Rtf = announcements[announceIndex];
                }
                catch (ArgumentException)
                {
                    richTextBoxAnnouncements.Text = announcements[announceIndex].StripRTF();
                }
                buttonPreviousAnnounce.Enabled = announcements.Count > 1;
            }
            buttonNextAnnounce.Enabled = false;
        }

        private void buttonView_Click(object sender, EventArgs e)
        {
            viewFile();
        }


        private int announceIndex;
        private List<string> announcements = new List<string>();

        private void buttonNextAnnounce_Click(object sender, EventArgs e)
        {
            announceIndex++;
            try
            {
                richTextBoxAnnouncements.Rtf = announcements[announceIndex];
            }
            catch (ArgumentException)
            {
                richTextBoxAnnouncements.Text = announcements[announceIndex].StripRTF();
            }
            if (announceIndex == announcements.Count - 1)
            {
                buttonNextAnnounce.Enabled = false;
            }
            if (announceIndex != 0)
            {
                buttonPreviousAnnounce.Enabled = true;
            }
        }

        private void buttonPreviousAnnounce_Click(object sender, EventArgs e)
        {
            announceIndex--;
            try
            {
                richTextBoxAnnouncements.Rtf = announcements[announceIndex];
            }
            catch (ArgumentException)
            {
                richTextBoxAnnouncements.Text = announcements[announceIndex].StripRTF();
            }
            if (announceIndex == 0)
            {
                buttonPreviousAnnounce.Enabled = false;
            }
            if (announceIndex != announcements.Count - 1)
            {
                buttonNextAnnounce.Enabled = true;
            }
        }

        private void checkedListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Dictionary<string, bool> boxItems = new Dictionary<string, bool>();
            foreach (var it in checkedListBoxIncludedata.Items)
            {
                if(!boxItems.ContainsKey(it.ToString()))
                    boxItems.Add(it.ToString(), checkedListBoxIncludedata.CheckedItems.Contains(it));
            }
            Select_Options so = new Select_Options(boxItems);
            so.ShowDialog();
            checkedListBoxIncludedata.Items.Clear();
            dataGridViewStudents.Columns.Clear();
            foreach (var se in so.items)
            {
                checkedListBoxIncludedata.Items.Add(se.Key, se.Value);
                if (se.Value)
                    dataGridViewStudents.Columns.Add(se.Key.Replace(" ", ""), se.Key);
            }
            so.Dispose();
        }

        private void buttonGenerate_Click(object sender, EventArgs e)
        {
            if (checkLocal())
            {
                return;
            }
            //Is the column data empty
            if (checkedListBoxIncludedata.CheckedItems.Count == 0)
            {
                MessageBox.Show("Please include some data to be generated/listed. Simply click on the DataIncludeBox and select the details of the student you want to retrieve", "No Data to Include", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                checkedListBoxIncludedata.Focus();
                return;
            }

            if ((comboBoxCriteria1.Text == "Level" || comboBoxCriteria2.Text == "Level") && toolLevelChooser.Text == "Choose Level")
            {
                MessageBox.Show(
                    "You have selected student level as a filter/criteria for the results. Please specify the level and try again.",
                    "No Level Specified", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                toolLevelChooser.Focus();
                return;
            }

            if ((comboBoxCriteria1.Text == "CGPA" || comboBoxCriteria2.Text == "CGPA") && toolComboSemester.Text == "Semester")
            {
                MessageBox.Show(
                    "You have seleected CGPA as a criteria. Please specify the current semester for accurate results.",
                    "Specify Current Semester", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                toolComboSemester.Focus();
                return;
            }

            //FIRST, let's get a student list based on the criteria supplied

            string query = CriteriaQuery(comboBoxCriteria1.Text);
            var regnos = Helper.ExecuteSelect(query);
            var regnos2 = comboBoxCriteria2.Text == "Select Filter"? null : Helper.ExecuteSelect(CriteriaQuery(comboBoxCriteria2.Text));
            if (regnos2 != null)
            {
                regnos = regnos.Where(regnos2.Contains).ToArray();
            }

            query = "SELECT ";
            //Let's do the select statement
            foreach (var item in checkedListBoxIncludedata.CheckedItems)
            {
                switch (item.ToString())
                {
                    case "Name":
                        query += "CONCAT_WS(' ', fName, lName, mName) as Name, ";
                        break;
                    case "Reg. No":
                        query += "students.regNo, ";
                        break;
                    case "Age":
                        query += "DATE_FORMAT(CURDATE(), '%Y%') - SUBSTR(students.dob, -4) as Age, ";
                        break;
                    case "Gender":
                        query += "IF(STRCMP(gender,'M'),'MALE','FEMALE') as gender, ";
                        break;
                    case "Department":
                        query += "dept, ";
                        break;
                    case "Email":
                        query += "email, ";
                        break;
                    case "Phone":
                        query += "phone, ";
                        break;
                    case "Level":
                        query += "level, ";
                        break;
                    case "Lga":
                        query += "lga, ";
                        break;
                    case "State":
                        query += "state, ";
                        break;
                    case "Course of Study":
                        query += "courseOfStudy, ";
                        break;
                }
            }
            //Let's remove the trailing ', '
            query = query.Remove(query.Length - 2);
            query += String.Format(" FROM students where regNo in ({0});",
                                   String.Join(",", regnos.Select(n => "'" + n[0] + "'")));
            var sresults = Helper.ExecuteSelect(query);

            //Let's add the results
            for (int i = 0; i < sresults.Length; i++)
            {
                var result = sresults[i];
                if (checkedListBoxIncludedata.CheckedItems.Contains("GPA") || checkedListBoxIncludedata.CheckedItems.Contains("CGPA"))
                {
                    var newarr = new string[result.Length + 1];
                    newarr[result.Length] = regnos[i][1];
                    Array.Copy(result, newarr, 0);
                }
                dataGridViewStudents.Rows.Add(result);
            }

            dataGridViewStudents.AutoResizeColumns();

            //Time to export to excel
        }

        private string CriteriaQuery(string val)
        {
            string query = "SELECT student_exams.regNo";
            switch (val)
            {
                case "GPA":
                    query +=
                        string.Format(", SUM(gp_score * units) as gpscore FROM student_exams" +
                                      " JOIN courses on student_exams.CourseCode = courses.CourseCode" +
                                      " WHERE academicYear != '{0}' OR (academicYear = '{0}' AND courses.semester != '{1}')" +
                                      " GROUP BY student_exams.regNo ORDER BY gpscore DESC", toolTextAcademicYear.Text, toolComboSemester.Text == "FIRST" ? "1" : "2");
                    break;
                case "CGPA":
                    query +=
                        string.Format(", SUM(gp_score * units) as gpscore FROM student_exams" +
                                      " JOIN courses on student_exams.CourseCode = courses.CourseCode" +
                                      " JOIN students on student_exams.regNo = students.regNo WHERE students.dept = '{2}'" +
                                      " AND academicYear = '{0}' AND courses.semester = '{1}'" +
                                      " GROUP BY student_exams.regNo ORDER by gpscore DESC", toolTextAcademicYear.Text, toolComboSemester.Text == "FIRST" ? "1" : "2", dd.DeptName);

                    break;
                case "Level":
                    query = string.Format("SELECT regNo FROM students WHERE level = '{0}' AND dept = '{1}'", toolLevelChooser.Text, dd.DeptName);
                    query += " ORDER BY " + comboBoxCriteria1.Text;
                    break;
                case "Possible Graduands":
                    query = string.Format("SELECT regNo FROM (SELECT regNo, MAX(gp_score) AS MaxGP" +
                                  " FROM student_exams GROUP BY regNo, CourseCode) AS student_exams" +
                                  " JOIN students on student_exams.regNo = students.regNo WHERE students.dept = '{0}'" +
                                  " GROUP BY regNo" +
                                  " HAVING MIN(MaxGP) > 0 ", dd.DeptName);
                    break;
            }
            query += " LIMIT " + numericLimit.Value;
            return query;
        }

        private bool checkLocal()
        {
            if (local)
            {
                var dr = MessageBox.Show("Cannot select students when on local mode. Do you want to go online now?",
                                "On Local Mode", MessageBoxButtons.YesNo, MessageBoxIcon.Asterisk);
                if (dr == DialogResult.Yes)
                {
                    if (Helper.AcademicYear != null)
                    {
                        local = false;
                        labelMode.Text = "Online";
                    }
                }
            }
            return local;
        }

        private void buttonTranscript_Click(object sender, EventArgs e)
        {
            if (checkLocal())
            {
                return;
            }
            if (toolComboSemester.Text == "Semester" || !toolComboSemester.Items.Contains(toolComboSemester.Text))
            {
                MessageBox.Show("The selected semester is not valid.", "Invalid Semester", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                toolComboSemester.Focus();
                return;
            }
            if (toolTextAcademicYear.Text == "" || toolTextAcademicYear.TextLength != 9)
            {
                MessageBox.Show("The academic year is not valid.", "Invalid Year", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                toolTextAcademicYear.Focus();
                return;
            }

            studentsData.Rows.Clear();
            if (radioButtonByLevel.Checked)
            {
                if (fbd.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        DirectoryInfo di = new DirectoryInfo(fbd.SelectedPath);
                        if (di.EnumerateFiles().Any())
                        {
                            var dr = MessageBox.Show(
                                "Selected folder is not empty. Are you sure you want to save in this directory? It is better to create new directories with the Student Level so as to remember file settings.",
                                "Folder Not Empty", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                            if (dr == DialogResult.No)
                            {
                                buttonTranscript.PerformClick();
                                return;
                            }
                        }
                        folderSave = di.FullName;
                        try
                        {
                            var thestudents =
                                Helper.selectResults(
                                    new[]
                                        {
                                            "CONCAT_WS(' ', fName, lName, mName) as Name", "regNo", "level",
                                            "courseOfStudy", "phone", "email"
                                        },
                                    "students", new[] {"level", "dept"},
                                    new[] {comboBoxTLevel.Text, dd.DeptName}, "");
                            for (int i = 0; i < thestudents.Length; i++)
                            {
                                var student = thestudents[i];
                                var sd = new StudentData
                                    {
                                        StudentName = student[0],
                                        RegNo = student[1],
                                        Level = student[2],
                                        CourseOfStudy = student[3],
                                        Phone = student[4],
                                        Email = student[5]
                                    };
                                GenerateStudentTranscript(sd, false);
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.WriteLog(ex);
                            MessageBox.Show(
                                "An error occurred while trying to retreive the students for the specified level. Please try again.",
                                "Retrieval Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            return;
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteLog(ex);
                        MessageBox.Show(
                            "An error occurred while trying to perform the requested action. Please try again.",
                            "Unknown Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return;                        
                    }

                    Process.Start("explorer.exe", folderSave);
                }
            }
            else
            {
                GenerateStudentTranscript(new StudentData{RegNo = textBoxTRegNo.Text}, true);
            }
        }

        private string folderSave;

        private void GenerateStudentTranscript(StudentData sd, bool save)
        {
            try
            {
                tempName = Application.UserAppDataPath + "\\" + sd.RegNo.Substring(5) + "transcript.xlsx";
                string semester = toolComboSemester.Text == "FIRST" ? "1" : "2";
                myProgress.Value = 0;
                //First let's get the student result details for current session and semester
                //Format: Regno, CourseCode, ca_score, exam_score, score, AcademicYear, CourseTitle
                progressLabel.Text = "Retriving Course Registration Data...";
                string squery = string.Format(
                    "SELECT student_exams.CourseCode, CourseTitle, units, gp_score, (units * gp_score)" +
                    " FROM student_exams JOIN courses on student_exams.CourseCode = courses.CourseCode" +
                    " WHERE regNo='{0}' AND academicYear='{1}' AND courses.semester='{2}'",
                    sd.RegNo, toolTextAcademicYear.Text, semester);
                var student = Helper.ExecuteSelect(squery);
                myProgress.Value += 15;
                //Let's geth the students personal details and get ready for insertion
                if (sd.StudentName == null)
                {
                    var thestudents =
                        Helper.selectResults(
                            new[] {"' ', fName, lName, mName) as Name,  regNo, level, courseOfStudy, phone, email"},
                            "students", new[] {"regNo"},
                            new[] {sd.RegNo}, "");
                    var studenta = thestudents[0];
                    sd = new StudentData
                        {
                            StudentName = studenta[0],
                            RegNo = studenta[1],
                            Level = studenta[2],
                            CourseOfStudy = studenta[3],
                            Phone = studenta[4],
                            Email = studenta[5]
                        };
                }
                myProgress.Value += 15;

                progressLabel.Text = "Generating transcript template...";
                //Let's generate the transcript template
                TranscriptGen tg = new TranscriptGen();
                tg.CreatePackage(tempName, "UNIVERSITY OF NIGERIA", dd.DeptName, dd.Faculty, toolTextAcademicYear.Text,
                                 sd.StudentName.ToUpperInvariant(), sd.RegNo,
                                 sd.Level[0] + "/" + dd.CourseDuration, sd.CourseOfStudy.ToUpperInvariant(),
                                 toolComboSemester.Text);
                myProgress.Value += 30;
                //Let's insert data
                //Get accumulated GP
                progressLabel.Text = "Retrieving student's result details...";
                string bquery = string.Format("SELECT SUM(gp_score * units), SUM(units)" +
                                              " FROM student_exams JOIN courses on student_exams.CourseCode = courses.CourseCode " +
                                              "WHERE regNo='{0}' AND academicYear != '{1}' " +
                                              "OR (regNo = '{0}' And academicYear = '{1}' AND courses.semester != '{2}')",
                                              sd.RegNo, toolTextAcademicYear.Text, semester);

                string[] bforward = Helper.ExecuteSelect(bquery)[0];
                myProgress.Value += 15;

                progressLabel.Text = "Inserting Results Data...";

                string[] gpdetails = Helper.InsertTranscriptData(tempName, "Sheet1", student, bforward[1], bforward[0], "A", 14);
                studentsData.Rows.Add(new[] { (studentsData.Rows.Count + 1).ToString(), sd.StudentName, sd.RegNo, sd.Phone, sd.Email, gpdetails[0], gpdetails[1] });

                myProgress.Value = 95;

                progressLabel.Text = save ? "Document Complete. Save now..." : "Transcript Generated for " + sd.RegNo;
                myProgress.Value = 95;
                if (save && sfd.ShowDialog() == DialogResult.OK)
                {
                    saveFile(sfd.FileName);
                }
                else
                {
                    saveFile(folderSave + "\\" + sd.RegNo.Substring(5) + "transcript.xlsx");
                }
                myProgress.Value = 100;
                if (save) progressLabel.Text = "Transcript Ready. Click Open to View";
                else
                {
                    progressLabel.Text = string.Format("Transcript generation for {0} complete.", sd.RegNo);
                }
                statusStrip1.Refresh();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(ex);
            }
        }

        private void toolLevelChooser_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach (Control c in groupBoxLevel.Controls)
            {
                RadioButton rb = c as RadioButton;
                if (rb.Name.Contains(toolLevelChooser.Text))
                {
                    rb.Checked = true;
                    break;
                }   
            }
        }

        private void comboBoxTLevel_SelectedIndexChanged(object sender, EventArgs e)
        {
            toolLevelChooser.Text = comboBoxTLevel.Text;
        }

        private void radioButtonByLevel_CheckedChanged(object sender, EventArgs e)
        {
            comboBoxTLevel.Enabled = radioButtonByLevel.Checked;
        }

        private void radioButtonByRegno_CheckedChanged(object sender, EventArgs e)
        {
            textBoxTRegNo.Enabled = radioButtonByRegno.Checked;
        }

        private void radioSemester2_CheckedChanged(object sender, EventArgs e)
        {
            toolComboSemester.Text = "SECOND";
        }

        private void radioSemester1_CheckedChanged(object sender, EventArgs e)
        {
            toolComboSemester.Text = "FIRST";
        }

        private void checkedListBoxIncludedata_Click(object sender, EventArgs e)
        {
            Dictionary<string, bool> boxItems = new Dictionary<string, bool>();
            foreach (var it in checkedListBoxIncludedata.Items)
            {
                if(!boxItems.ContainsKey(it.ToString()))
                    boxItems.Add(it.ToString(), checkedListBoxIncludedata.CheckedItems.Contains(it));
            }
            Select_Options so = new Select_Options(boxItems);
            so.ShowDialog();
            checkedListBoxIncludedata.Items.Clear();
            dataGridViewStudents.Columns.Clear();
            foreach (var se in so.items)
            {
                checkedListBoxIncludedata.Items.Add(se.Key, se.Value);
                if(se.Value)
                    dataGridViewStudents.Columns.Add(se.Key.Replace(" ", ""), se.Key);
            }
            so.Dispose();
        }

        private void buttonPrintTrans_Click(object sender, EventArgs e)
        {
            PrintDude(24);
        }

        private void PrintDude(int cnt)
        {
            if (savedFileName == null)
            {
                MessageBox.Show("It seems the file has not been saved yet. Please save the file first and try again.",
                                "File Missing", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                return;
            }
            var excelApp = new Microsoft.Office.Interop.Excel.Application(); // Creates a new Excel Application
            excelApp.Visible = true; // Makes Excel visible to the user.

            Workbook excelWorkbook = null;
            excelApp.DefaultFilePath = savedFileName;
            // Open the Workbook:
            excelWorkbook = excelApp.Workbooks.Open(savedFileName, 0, false, 5, "", "", false, XlPlatform.xlWindows, "",
                                                    true,
                                                    false, 0, true, false, false);

            var ws = excelWorkbook.Worksheets[1] as Worksheet;

            // The following gets the Worksheets collection
            var excelSheet = excelWorkbook.Worksheets[1] as Worksheet;

            excelSheet.PrintOutEx(1, 1, 1, false, null, false, false, false, false);
            excelSheet.get_Range("B12", "B" + cnt).Activate();

            bool userDidntCancel =
                excelApp.Dialogs[XlBuiltInDialog.xlDialogPrint].Show(2, 1, 4, 1, false, false);

            if (userDidntCancel)
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();

                //Marshal.FinalReleaseComObject(excelSheet);

                excelWorkbook.Close(false, Type.Missing, Type.Missing);
                Marshal.FinalReleaseComObject(excelWorkbook);

                excelApp.Quit();
                Marshal.FinalReleaseComObject(excelApp);
            }
        }

        private void buttonSaveTrans_Click(object sender, EventArgs e)
        {
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                saveFile(sfd.FileName);
            }
        }

        private int timerCount;
        /// <summary>
        /// The amount of time to wait before session is locked. Since timer runs every 3 seconds, this means 180 seconds or three minutes
        /// </summary>
        private int timerTime = 60;

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            if (announcements.Count != Helper.GetAnnouncementCount("department", dd.DeptName, UserRoles.ExamOfficer))
            {
                LoadAnnouncements(true);
            }
            if (acts.Count != Helper.GetActivitiesCount("department", dd.DeptName))
            {
                LoadActivities(true);
            }
            timerCount++;
            if (timerCount == timerTime)
            {
                toolButtonLock.PerformClick();
            }
            timer1.Enabled = true;
        }

        private void toolButtonLock_Click(object sender, EventArgs e)
        {
            Locked lc = new Locked(username);
            Visible = false;
            lc.UnlockEvent += (o, args) => { Visible = args.Unlocked; timerCount = 0; };
            lc.ShowDialog();
        }

        private void leaveAMessageForThisStudentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                string name =
                    Helper.selectResults(new[] {"CONCAT_WS(' ', fName, lName) as studentName"}, "students",
                                          new[] {"regNo"}, new[] {textBoxTRegNo.Text}, "")[0][0];
                MessageUser mu = new MessageUser(username, textBoxTRegNo.Text, name);
                mu.ShowDialog();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(ex);
                MessageBox.Show("An error occurred while trying to contact the specified user. " + ex.Message,
                                "User Retrieval Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private void contactLecturerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                string courseCode = studentsData.SelectedRows[0].Cells["CourseCode"].Value.ToString();
                string dept =
                    Helper.selectResults(new[] {"dept"}, "students", new[] {"regNo"}, new[] {textBoxTRegNo.Text}, "")[0
                        ][0];
                string[] lecturerCourse =
                    Helper.selectResults(
                        new[] {"username", "CONCAT_WS(' ', title, firstName, lastName) as lecturerName"},
                        "lecturer_courses",
                        string.Format(
                            "JOIN lecturers on lecturers.username = lecturer_courses.username WHERE department = '{0}' AND CourseCode = '{1}' AND academicYear = '{2}'",
                            dept, courseCode, Helper.AcademicYear))[0];
                MessageUser mu = new MessageUser(username, lecturerCourse[0], lecturerCourse[1]);
                mu.ShowDialog();
            }

            catch (Exception ex)
            {
                Logger.WriteLog(ex);
                MessageBox.Show("An error occurred while trying to contact the specified user. " + ex.Message,
                                "User Retrieval Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private void fbd_HelpRequest(object sender, EventArgs e)
        {

        }

        private SoundPlayer sp;

        private void notifyIcon1_BalloonTipShown(object sender, EventArgs e)
        {
            sp = sp ?? new SoundPlayer();
            sp.Stream = Properties.Resources._2cool;
            sp.Play();
        }

        private void notifyIcon1_BalloonTipClicked(object sender, EventArgs e)
        {
            this.tabControl1.SelectTab(0);
            this.BringToFront();
            this.Focus();
            if (WindowState == FormWindowState.Minimized)
            {
                WindowState = FormWindowState.Normal;
            }
        }

        private void buttonViewinExcel_Click(object sender, EventArgs e)
        {
            myProgress.Value = 0;
            BlankExcel be = new BlankExcel();
            tempName = Application.UserAppDataPath + "\\" + comboBoxCriteria1.Text + "students.xlsx";
            be.CreatePackage(tempName);
            string[] headers = new string[dataGridViewStudents.ColumnCount];
            for (int i = 0; i < dataGridViewStudents.ColumnCount; i++)
            {
                headers[i] = dataGridViewStudents.Columns[i].HeaderText;
            }

            List<string[]> data = new List<string[]>();
            for (int i = 0; i < dataGridViewStudents.RowCount; i++)
            {
                int count = dataGridViewStudents.Rows[i].Cells.Count;
                string[] cells = new string[count];
                for (int j = 0; j < count; j++)
                {
                    cells[j] = dataGridViewStudents.Rows[i].Cells[j].Value.ToString();
                }
                data.Add(cells);
            }

            Helper.GenerateExcel(headers, data, tempName);

            progressLabel.Text = "Document Complete. Save now...";
            myProgress.Value = 95;
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                saveFile(sfd.FileName);
            }
            myProgress.Value = 100;
            progressLabel.Text = "Excel Document Ready.";
        }

        private void buttonPrintReport_Click(object sender, EventArgs e)
        {
            PrintDude(dataGridViewStudents.RowCount + 1);

/*
            MessageBox.Show("This function has not been implemented yet.", "Unimplemented function requested",
                MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
*/
        }

        private void btnviewList_Click(object sender, EventArgs e)
        {
            viewFile();
        }
    }
}
