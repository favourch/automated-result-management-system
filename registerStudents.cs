using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ARMS
{
    public partial class registerStudents : Form
    {
        public registerStudents()
        {
            InitializeComponent();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            int count = 0;
            if (dg.Rows[0].IsNewRow)
            {
                MessageBox.Show("There is no student to be registered.", "e-Campus Manager", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            var result = MessageBox.Show("The student's registration number cannot be changed\nafter registration. \nClick Ok to continue or Cancel to confirm", "e-Campus Manager", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
            if (result.ToString() == "Cancel")
                return;


            
            foreach (DataGridViewRow dgrow in dg.Rows)
            {
                string[] info = new string[13];
                if (dgrow.IsNewRow)
                {
                    if (count == 1)
                    {
                        MessageBox.Show("The student has been registered to the department.", "e-Campus Manager", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else if (count > 1)
                    {
                        MessageBox.Show("The students has been registered to the department.", "e-Campus Manager", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                    else if (count == 0)
                    {
                        MessageBox.Show("There is no student to be registered.", "e-Campus Manager", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    } 
                    return;
                }
                int value = Helper.checkSchoolFees(dgrow.Cells[0].Value.ToString());
                if (value < 1)
                {
                    MessageBox.Show("This student cannot be registered.\nStudent not cleared in the Bursary department.", "e-Campus Manager", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                info[0] = dgrow.Cells[0].Value.ToString();
                info[1] = dgrow.Cells[2].Value.ToString();
                info[2] = dgrow.Cells[1].Value.ToString();
                info[3] = dgrow.Cells[3].Value.ToString();
                info[4] = dgrow.Cells[4].Value.ToString();
                info[5] = dgrow.Cells[5].Value.ToString();
                info[6] = dgrow.Cells[6].Value.ToString();
                info[7] = dgrow.Cells[7].Value.ToString();
                info[8] = dgrow.Cells[8].Value.ToString();
                info[9] = dgrow.Cells[9].Value.ToString();
                info[10] = dgrow.Cells[10].Value.ToString();
                info[11] = dgrow.Cells[11].Value.ToString();
                info[12] = dgrow.Cells[12].Value.ToString();
               
               Helper.dbInsert("students", new[] { "regNo", "fName", "lName", "mName", "phone", "email", "faculty", "dept", "level", "session", "semester", "gender", "degree" }, info);
               count += 1;
                
            }
            if (count == 1)
            {
                MessageBox.Show("The student has been registered to the department", "e-Campus Manager", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else if (count > 1)
            {
                MessageBox.Show("The students has been registered to the department", "e-Campus Manager", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else if (count==0)
            {
                MessageBox.Show("There is no student to registered", "e-Campus Manager", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        
        private void dg_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (btnSave.Enabled == false)
                btnSave.Enabled = true;
        }
    }
}
