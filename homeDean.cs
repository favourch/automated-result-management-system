using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Media;
using System.Windows.Forms;

namespace ARMS
{
    public partial class homeDean : Form
    {
        private string faculty;
        private string username;
        private bool local;

        public homeDean(string username, string formalName, string faculty, bool local)
        {
            this.local = local;
            InitializeComponent();
            labelMode.Text = local ? "Local Mode" : "Online";
            this.username = username;
            this.faculty = faculty;
            labelWelcome.Text = string.Format("Good day {0}.", formalName); this.MouseMove += app_MouseMove;
        }

        void app_MouseMove(object sender, MouseEventArgs e)
        {
            timerCount = 0;
        }

        private string[] cRow;
        private List<string[]> srows = new List<string[]>();
        private Dictionary<string, LecturerDetails> allLecturers = new Dictionary<string, LecturerDetails>();
        private string lastStaffCell;
        //private Dictionary<string, Course> courses = new Dictionary<string, Course>();

        private void toolButtonLock_Click(object sender, EventArgs e)
        {
            Locked lc = new Locked(username);
            Visible = false;
            lc.UnlockEvent += (o, args) => { Visible = args.Unlocked; timerCount = 0; };
            lc.ShowDialog();
        }

        private void btnAnnouncements_Click(object sender, EventArgs e)
        {
            Announcer an = new Announcer(username, faculty, "");
            an.ShowDialog();
        }

        private void buttonAddStaff_Click(object sender, EventArgs e)
        {
            //Let's generate a random staff code
            Random rnd = new Random(DateTime.Now.Millisecond);

            var department =
                Helper.selectResults(new[] {"shortName", "faculty", "courseDuration"},
                                      "departments", new[] {"deptName"}, new[] {comboBoxHDept.Text},
                                      "");
            DeptDetails dd = new DeptDetails
                {
                    DeptName = comboBoxHDept.Text,
                    ShortName = department[0][0],

                    Faculty = department[0][1],
                    CourseDuration = department[0][2]
                };

            //Let's retrieve a list of current staff codes
            var staffcodes = allLecturers.Select(n => n.Value.StaffCode).ToList();
            string staffCode = dd.ShortName + rnd.Next(1000, 9999);

            while (staffcodes.Contains(staffCode))
            {
                staffCode = dd.ShortName + rnd.Next(1000, 9999);
            }
            LecturerReg lr = new LecturerReg(username, dd.DeptName, dd.Faculty, staffCode);
            lr.ShowDialog();
            LoadStaff();
        }

        private void LoadStaff()
        {
            dataGridViewStaff.Rows.Clear();
            comboBoxLecturers.Items.Clear();
            allLecturers.Clear();
            //Load Lecturers
            int count = 0;
            foreach (
                string[] s in
                    Helper.selectResults(
                        new[] {"lecturers.username as username", "title", "lastName", "firstName", "email", "staffCode"},
                        "lecturers",
                        new[] {"faculty"}, new[] {faculty},
                        "JOIN users ON lecturers.username = users.username")
                )
            {
                count++;
                LecturerDetails ld = new LecturerDetails
                    {
                        Department = comboBoxHDept.Text,
                        UserName = s[0],
                        Title = s[1],
                        LastName = s[2],
                        FirstName = s[3],
                        Email = s[4],
                        StaffCode = s[5]
                    };
                var lname = string.Format("{0} {1} {2}", ld.Title, ld.LastName, ld.FirstName);
                comboBoxLecturers.Items.Add(lname);
                allLecturers.Add(lname, ld);
                dataGridViewStaff.Rows.Add(new object[] {count, ld.UserName, lname, ld.Email, ld.StaffCode});
            }
        }

        private void buttonEditStaff_Click(object sender, EventArgs e)
        {
            var selectedRows = dataGridViewStaff.SelectedRows;
            if (selectedRows.Count == 0) return;
            var cell = selectedRows[0].Cells[0];
            dataGridViewStaff.CurrentCell = cell;
            dataGridViewStaff.BeginEdit(false);
        }

        private void buttonDeleteStaff_Click(object sender, EventArgs e)
        {
            var curRow = dataGridViewStaff.SelectedRows[0];
            if (curRow != null)
            {
                var dr = MessageBox.Show(
                    "Are you sure you want to remove this staff? This user will no longer be able to login to the system and any existing records on the system will be archived automatically. Press yes to continue and remove this staff or no to cancel this action.",
                    "Delete Staff", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (dr == DialogResult.Yes)
                {
                    LecturerDetails lecturer = allLecturers[curRow.Cells["StaffCode"].Value.ToString()];
                    var usrName = lecturer.UserName;
                    var result = Helper.Update("users", new[] { "deleted" }, new[] { "Y" }, new[] { "username" },
                                                new[] { usrName }, "");
                    if (result == 1)
                    {
                        MessageBox.Show("The selected staff has been deleted successfully.",
                                        "Staff Removed Successfully", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Activity act = new Activity
                        {
                            Department = lecturer.Department,
                            Faculty = lecturer.Faculty,
                            Username = username,
                            Text =
                                string.Format("The Dean of Faculty has Removed Staff {0} from the department of {1}.",
                                              lecturer.FormalName, lecturer.Department)
                        };
                        Helper.logActivity(act);
                        dataGridViewStaff.Rows.Remove(curRow);
                        LoadStaff();
                    }
                    else
                    {
                        MessageBox.Show("An error occurred while trying to remove the specified staff. Please try again");
                    }
                }
            }
        }

        private void dataGridViewStaff_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                studentsData.CancelEdit();
                Logger.WriteLog(ex.Source + ex.Message, ex.StackTrace);
                MessageBox.Show("It seems there was an error in your input. Please try again.", "Incorrect Input",
                                MessageBoxButtons.OK);
            }
        }

        private void dataGridViewStaff_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            lastStaffCell = dataGridViewStaff.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();
        }

        private void buttonSaveStaff_Click(object sender, EventArgs e)
        {

        }

        private void buttonAssignHOD_Click(object sender, EventArgs e)
        {
            assignRole(UserRoles.HOD);
        }

        private void assignRole(string role)
        {
            if (buttonAssignHOD.Text == "Un&Assign")
            {
                var dr =
                    MessageBox.Show(
                        string.Format(
                            "The {1} role is already assigned to {0}. If you want to unassign this role, Click Retry, otherwise Click Abort/Ignore.",
                            currentLecturer.FormalName, role), "Role Already Assigned",
                        MessageBoxButtons.AbortRetryIgnore,
                        MessageBoxIcon.Warning);
                if (dr == DialogResult.Retry)
                {
                    int d = Helper.DbDelete("roles", new[] {"username", "role"},
                                            new[] {currentLecturer.UserName, role});
                    if (d == -1)
                    {
                        MessageBox.Show("An error occured while trying to unassign this role. Please try again.");
                        return;
                    }
                    else
                    {
                        MessageBox.Show(
                            string.Format(
                                "{0} has been unassigned as {1} of {2}.",
                                currentLecturer.FormalName, role, comboBoxHDept.Text), "Role UnAssigned Successfully",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Information);

                        Helper.logActivity(
                            new Activity
                                {
                                    Department = comboBoxHDept.Text,
                                    Faculty = faculty,
                                    Username = username,
                                    Text =
                                        string.Format(
                                            "The Dean of Faculty has unassigned {0} as the Head of Department of {2}.",
                                            currentLecturer.NiceName, role, comboBoxHDept.Text)
                                });

                    }
                }
            }
            else
            {
                int di = Helper.dbInsert("roles", new[] {"username", "role"},
                                         new[] {currentLecturer.UserName, role});
                if (di == -1)
                {
                    MessageBox.Show("An error occured while trying to assign this role. Please try again.");
                }
                else
                {
                    MessageBox.Show(
                        string.Format(
                            "The {1} role has been assigned successfully to {0}.",
                            currentLecturer.FormalName, role), "Role Assigned Successfully", MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                    Helper.logActivity(
                        new Activity
                            {
                                Department = comboBoxHDept.Text,
                                Faculty = faculty,
                                Username = username,
                                Text =
                                    string.Format(
                                        "The Dean of Faculty has assigned {0} as the Head of Department of {2}.",
                                        currentLecturer.NiceName, role, comboBoxHDept.Text)
                            });

                }
            }
        }

        private void comboBoxDepartments_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboCourse.Items.Clear();
            toolStripComboCourse.Items.Clear();
            foreach (
                string[] s in
                    Helper.selectResults(new[] {"CourseCode", "CourseTitle", "units", "semester"}, "courses",
                                          new[] {"department"}, new[] {comboBoxDepartments.Text}, "")
                )
            {
                try
                {
                    comboCourse.Items.Add(s[0]);
                    toolStripComboCourse.Items.Add(s[0]);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(ex.Message + Environment.NewLine + ex.StackTrace,
                                            DateTime.Now.ToShortDateString());
                }
            }
        }

        private void clearStudentData()
        {
            studentTotalScores = 0;
            studentsData.Rows.Clear();
            Acount = Bcount = Ccount = Dcount = Ecount = Fcount = MRcount = NRcount = 0;
            labelA.Text = string.Format("As: {0}", Acount);
            labelB.Text = string.Format("Bs: {0}", Bcount);
            labelC.Text = string.Format("Cs: {0}", Ccount);
            labelD.Text = string.Format("Ds: {0}", Dcount);
            labelE.Text = string.Format("Es: {0}", Ecount);
            labelF.Text = string.Format("Fs: {0}", Fcount);
            labelAR.Text = string.Format("Ommitted Results: {0}", MRcount);
            labelNR.Text = string.Format("No Results: {0}", NRcount);
        }

        private void comboCourse_SelectedIndexChanged(object sender, EventArgs e)
        {

            clearStudentData();
            //load all results for selected course for the current academic session
            var results = Helper.selectResults(new[] {"regNo", "CourseCode", "ca_score", "exam_score"}, "student_exams",
                                                new[] {"academicYear", "CourseCode"},
                                                new[] {toolTextAcademicYear.Text, comboCourse.Text}, "");
            if (results.Length == 0)
            {
                btnApprove.ColorBase = btnReject.ColorBase = Color.Gainsboro;
                return;
            }
            var orclause = String.Join(" OR regNo = ", results.Select(n => "'" + n[0] + "'").ToArray());
            var sresults =
                Helper.selectResults(
                    new[] {"CONCAT(lName, ' ', fName, ' ', mName) as studentName", "regNo", "dept", "level"},
                    "students", " WHERE regNo = " + orclause);
            int index = 0;
            for (int i = 0; i < results.Length; i++)
            {
                var student = results[i];
                if (sresults[i][2] != comboBoxDepartments.Text)
                {
                    //not needed at department level. Approved at faculty
                    continue;
                }
                index++;
                var sd = new StudentData
                    {
                        StudentName = sresults[i][0],
                        RegNo = sresults[i][1],
                        Dept = sresults[i][2],
                        Level = sresults[i][3],
                        CaMark = student[2],
                        ExamMark = student[3]
                    };
                switch (sd.TotalMark)
                {
                    case "MR":
                        MRcount++;
                        labelAR.Text = string.Format("Ommitted Results: {0}", MRcount);
                        break;
                    case "NR":
                        NRcount++;
                        labelNR.Text = string.Format("No Results: {0}", NRcount);
                        break;
                }
                int score = (int) sd.TotalScore;
                studentTotalScores += score;
                IncreaseScoreCount(score);
                studentsData.Rows.Add(new object[]
                    {index.ToString(CultureInfo.InvariantCulture), sd.StudentName, sd.RegNo, sd.CaMark, sd.ExamMark, sd.TotalMark, sd.GradeLetter});
                studentsData.Update();
            }
            int prating = studentTotalScores/(studentsData.RowCount);
            //labelPerformance.Text = Globals.GetRating(prating);

            var result =
                Helper.selectResults(new[] {"fapproved"}, "lecturer_courses",
                                      new[] {"academicYear", "department", "CourseCode"},
                                      new[] {toolTextAcademicYear.Text, comboBoxDepartments.Text, comboCourse.Text}, "")
                    [0][0];
            labelApproved.Text = result == "Y" ? "Yes" : "No";
            btnApprove.Enabled = btnReject.Enabled = result != "Y";
            if (btnApprove.Enabled)
                btnApprove.ColorBase = btnReject.ColorBase = Color.FromArgb(192, 255, 192);
            else btnApprove.ColorBase = btnReject.ColorBase = Color.Gainsboro;
        }

        private int Acount;
        private int Bcount;
        private int Ccount;
        private int Dcount;
        private int Ecount;
        private int Fcount;
        private int MRcount;
        private int NRcount;
        private int cIndex;
        private int announceIndex;
        private int studentTotalScores;
        private List<string> announcements = new List<string>();
        private LecturerDetails currentLecturer;

        private void IncreaseScoreCount(int score)
        {
            if (score >= 70 && score <= 100)
            {
                Acount++;
                labelA.Text = string.Format("As: {0}", Acount);
            }
            else if (score >= 60 && score < 70)
            {
                Bcount++;
                labelB.Text = string.Format("Bs: {0}", Bcount);
            }
            else if (score >= 50 && score < 60)
            {
                Ccount++;
                labelC.Text = string.Format("Cs: {0}", Ccount);
            }
            else if (score >= 45 && score < 50)
            {
                Dcount++;
                labelD.Text = string.Format("Ds: {0}", Dcount);
            }
            else if (score >= 40 && score < 45)
            {
                Ecount++;
                labelE.Text = string.Format("Es: {0}", Ecount);
            }
            else
            {
                Fcount++;
                labelF.Text = string.Format("Fs: {0}", Fcount);
            }
        }

        private void btnApprove_Click(object sender, EventArgs e)
        {
            //Approval Codes here
            var dr = MessageBox.Show(
                string.Format(
                    "Are you sure you want to approve the results for {0}. Once approved this action is final unless it is rejected by the Faculty. Do you want to continue?",
                    comboCourse.Text), "Confirm Approval", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (dr == DialogResult.Yes)
            {
                ApproveCourse(comboCourse.Text, new[]{"fapproved", "dapproved"}, true);
            }
        }

        private void btnReject_Click(object sender, EventArgs e)
        {
            //Rejection codes here
            var dr = MessageBox.Show(
                string.Format(
                    "Are you sure you want to reject the results for {0}. Once rejected the lecturer is able to re-submit the results for re-evaluation. Do you want to continue?",
                    comboCourse.Text), "Confirm Disapproval", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (dr == DialogResult.Yes)
            {
                ApproveCourse(comboCourse.Text, new[]{"fapproved", "dapproved"}, false);
            }
        }

        private void ApproveCourse(string courseCode, string[] approveLevel, bool approved)
        {
            int update = Helper.Update("lecturer_courses", approveLevel, new[] { approved ? "Y" : "N", approved ? "Y" : "N" },
                                        new[] {"academicYear", "department", "CourseCode"},
                                        new[] {toolTextAcademicYear.Text, comboBoxDepartments.Text, courseCode}, "");
            if (update == 1)
            {
                MessageBox.Show(
                    string.Format("The results for {0} has been {1} by the Dean.", courseCode, approved ? "approved." : "rejected."),
                    "Results " + (approved ? "Approved" : "Rejected"), MessageBoxButtons.OK, MessageBoxIcon.Information);

                btnApprove.Enabled = btnReject.Enabled = false;
                btnApprove.ColorBase = btnReject.ColorBase = Color.Gainsboro;

                labelApproved.Text = approved ? "Yes" : "No";

                Helper.logActivity(new Activity
                {
                    Department = comboBoxDepartments.Text,
                    Faculty = faculty,
                    Text =
                        string.Format(
                            "The results for {0} was {2} by the Dean of Faculty.",
                            courseCode, "", approved ? "approved" : "rejected"),
                    Username = username
                });
            }
            else
            {
                var dr =
                    MessageBox.Show(
                        string.Format("An error occurred while {0} the results. Do you want to try again.",
                                      approved ? "approving" : "rejecting"),
                        string.Format("Results {0} Error", approved ? "Approval" : "Rejection"),
                        MessageBoxButtons.RetryCancel, MessageBoxIcon.Error);
                if (dr == DialogResult.Retry)
                {
                    ApproveCourse(courseCode, approveLevel, approved);
                }
            }
        }

        private void homeDean_Load(object sender, EventArgs e)
        {
            tt = new Timer{Interval = 1000, Enabled = true};
            tt.Tick += LoadForm;
        }

        private Timer tt;

        private void LoadForm(object state, EventArgs eventArgs)
        {
            tt.Enabled = false;
            LoadActivities(false);
            LoadAnnouncements(false);

            //Load Departments
            var depts = Helper.selectResults(new[] {"deptName"}, "departments",
                                              string.Format("WHERE faculty = '{0}'", faculty));
            foreach (var dept in depts)
            {
                comboBoxDepartments.Items.Add(dept[0]);
                //comboBoxDepts.Items.Add(dept[0]);
                comboBoxHDept.Items.Add(dept[0]);
            }

            LoadStaff();
            timer1.Enabled = true;
        }

        List<Activity> acts = new List<Activity>();

        private void LoadActivities(bool notify)
        {
            foreach (var ac in Helper.GetActivities("faculty", faculty))
            {
                if (ac.IsIn(acts))
                {
                    continue;
                }
                acts.Add(ac);
                dataGridActivity.Rows.Insert(0, new[] { ac.ActivityDate.ToLongDateString(), ac.Text });
                if (notify)
                {
                    notifyIcon1.ShowBalloonTip(3000, "New Activity", ac.Text, ToolTipIcon.Info);
                }
            }
        }

        private void LoadAnnouncements(bool notify)
        {
            foreach (var an in Helper.GetAnnouncements("faculty", faculty, UserRoles.Dean))
            {
                if (announcements.Contains(an))
                {
                    continue;
                }
                announcements.Add(an);
                if (notify)
                {
                    notifyIcon1.ShowBalloonTip(3000, "New Annoucement Received", an.StripRTF().Excerpt(40), ToolTipIcon.Info);
                }
            }
            if (announcements.Count == 0)
            {
                buttonPreviousAnnounce.Enabled = false;
            }
            else
            {
                announceIndex = announcements.Count - 1;
                try
                {
                    richTextBoxAnnouncements.Rtf = announcements[announceIndex];
                }
                catch (ArgumentException)
                {
                    richTextBoxAnnouncements.Text = announcements[announceIndex].StripRTF();
                }
                buttonPreviousAnnounce.Enabled = announcements.Count > 1;
            }
            buttonNextAnnounce.Enabled = false;
        }

        private void comboBoxHDept_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadStaff();
        }

        private void comboBoxLecturers_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                var combo = (ComboBox)sender;
                currentLecturer = allLecturers[combo.Text];
                string rCount = Helper.selectResults(new[] {"COUNT(*)"}, "roles", new[] {"username", "role"}, new[] {currentLecturer.UserName, "HOD"}, "")[0][0];
                buttonAssignHOD.Text = Convert.ToInt32(rCount) > 0 ? "Un&Assign" : "&Assign";
            }
            catch (Exception ex)
            {
                Logger.WriteLog(ex);
            }
        }

        /// <summary>
        /// The amount of time to wait before session is locked. Since timer runs every 3 seconds, this means 180 seconds or three minutes
        /// </summary>
        private int timerTime = 60;


        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            if (announcements.Count != Helper.GetAnnouncementCount("faculty", faculty, UserRoles.Dean))
            {
                LoadAnnouncements(true);
            }
            if (acts.Count != Helper.GetActivitiesCount("faculty", faculty))
            {
                LoadActivities(true);
            }
            timerCount++;
            if (timerCount == timerTime)
            {
                toolButtonLock.PerformClick();
            }
            timer1.Enabled = true;
        }

        private int timerCount;

        private void notifyIcon1_BalloonTipClicked(object sender, EventArgs e)
        {
            tabControl1.SelectTab(0);
            this.BringToFront();
            this.Focus();
            if (WindowState == FormWindowState.Minimized)
            {
                WindowState = FormWindowState.Normal;
            }
        }

        private SoundPlayer sp;

        private void notifyIcon1_BalloonTipShown(object sender, EventArgs e)
        {
            sp = sp ?? new SoundPlayer();
            sp.Stream = Properties.Resources._2cool;
            sp.Play();
        }
    }
}
